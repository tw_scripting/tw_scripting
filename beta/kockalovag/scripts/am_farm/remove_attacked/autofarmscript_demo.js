javascript:

/*This is a demo script.
It brings you onto the am_farm screen if not already there.
Then it sets up the shortcuts containing farm_script_version:
If you keep the mouse down it will repeatedly c-farm the first village in the list then removes the element. 
The repetition is 4-5 times a second.
The user has to be there, and hold down the mouse button.
If you release the button, the autosend stops.*/

auto_repeat = 1;
interval = 250;
farm_script_version="1.5";

function autoFarm() {
	if (autoFarmOn) {
		advancedFarmScript();
		setTimeout( function() {
			console.log("autoFarm...");
            autoFarm();
		}, interval);
	} else {
		console.log("Stopped autoFarm");
	}
}

function startAutoFarm() {
	autoFarmOn = true;
	autoFarm();
}

function stopAutoFarm() {
	autoFarmOn = false;
}

function setupScriptLinks() {
	if (auto_repeat) {
		autoFarmOn = false;
		mousePressed = false;
		
		var quickBars = $("ul.quickbar");
		var scriptLinks = $("a", quickBars);
		var farmScriptLinks = scriptLinks.filter( function(idx) {
			return this.href.indexOf("farm_script_version") != -1;
		});
		$(farmScriptLinks).mousedown( function(){ 
			mousePressed = true;
			startAutoFarm();
		});
		$(farmScriptLinks).mouseup( function(){ 
			mousePressed = false;
			stopAutoFarm();
		});
		$(farmScriptLinks).mouseout( function(){ 
			stopAutoFarm();
		});
	}
	autoFarmSetupReady = true;
}

function advancedFarmScript() {
	var f = $("a.farm_icon_c")[0];
	f.click();
	$(f).closest("tr").remove();
}

if (game_data.screen!="am_farm") {
	UI.InfoMessage("This is a farmscript...", 4000);
	self.location=game_data.link_base_pure.replace(/screen\=\w*/i, "screen=am_farm");
} else if (typeof(autoFarmSetupReady) == "undefined") {
	setupScriptLinks();
	UI.InfoMessage("AutoFarm ready, click on the link again and keep mouse-button down", 3000);
}

void(0 );
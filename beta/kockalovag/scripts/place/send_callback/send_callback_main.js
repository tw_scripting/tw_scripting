javascript:

var doc=document;
var win=(window.frames.length>0)?window.main:window;

var unitsThere;
var unitsArr;
var unitInputs;

function fillCoords()
{		
	var items=document.getElementsByTagName("li");
	var i = 0;
	var found = false;
	while(!found){
		actualElement = items.item(i).innerHTML;
		if (actualElement.search(searchedListName) != -1) {
			found = true;
		} else {
			i++;
		}
	}

	if (!found){
		alert("Nem �ll�tott�l be seg�dscriptet!");
	}else{
		var coordSource = unescape(items.item(i).innerHTML);
		var begin = coordSource.indexOf(searchedListName+"(") + searchedListName.length;
		var end=coordSource.indexOf(")"+searchedListName);
		coordSource = coordSource.substring(begin+1,end);
	}
	
	var coords=coordSource;
	coords=coords.split(" ");
	var index=Math.round(Math.random()*(coords.length-1));
	
	var coord=coords[index];
	coord=coord.split("|");
	
	doc.forms[0].x.value=coord[0];
	doc.forms[0].y.value=coord[1];
}


function collectUnitsThere()
{
	var links = doc.forms[0].getElementsByTagName("a");
	unitsThere = new Array();
	
	for (var i = 0; i < links.length; i++) {
		if (links[i].href.indexOf('javascript:insertUnit') != -1) {
			var str = links[i].innerHTML;
			var len = str.indexOf(")") - str.indexOf("(") - 1;
			str = str.substr( str.indexOf("(")+1, len);
			unitsThere.push( parseInt(str) );
		}
	}	
}

function collectUnits()
{
	unitsArr = new Array();
	
	unitsArr.push( units.nSpear );
	unitsArr.push( units.nSword );
	unitsArr.push( units.nAxe );
	unitsArr.push( units.nArcher );
	
	unitsArr.push( units.nSpy );
	unitsArr.push( units.nLight );
	unitsArr.push( units.nMarcher );
	unitsArr.push( units.nHeavy );
	
	unitsArr.push( units.nRam );
	unitsArr.push( units.nCatapult );
	
	unitsArr.push( units.nKnight );
	unitsArr.push( units.nSnob );
}

function collectUnitInputs()
{
	unitInputs =  new Array();
	
	unitInputs.push( doc.forms[0].spear );
	unitInputs.push( doc.forms[0].sword );
	unitInputs.push( doc.forms[0].axe )
	unitInputs.push( doc.forms[0].archer );
	
	unitInputs.push( doc.forms[0].spy );
	unitInputs.push( doc.forms[0].light );
	unitInputs.push( doc.forms[0].marcher );
	unitInputs.push( doc.forms[0].heavy );
	
	unitInputs.push( doc.forms[0].ram );
	unitInputs.push( doc.forms[0].catapult );
	
	unitInputs.push( doc.forms[0].knight );
	unitInputs.push( doc.forms[0].snob );
}
	
function insertSentUnit( inputElement, num )
{
	if (clearTheOthers || (num > 0)) {
		if (addToCurrent && (inputElement.value != "")) {
			insertUnit(inputElement, parseInt(inputElement.value) + num);
		} else {
			insertUnit(inputElement, num);
		}
	}
}

function insertSentUnits(){
	for (var i = 0; i < 12; i++) {
		var sentNum = unitsArr[i];
		if (i < 10) { /*We send only one Knight(idx: 10) or Snob(idx: 11)*/
			sentNum *= unitMultiplicator;
		}
		insertSentUnit( unitInputs[i], sentNum );
	}
}

function insertCallbackUnit( inputElement, num )
{
	insertUnit(inputElement, num);
}

function insertCallbackUnits()
{
	for (var i = 0; i < 12; i++) {
		var callbackNum;
		if (unitsArr[i] == 0) {
			callbackNum = 0;
		} else {
			callbackNum = unitsThere[i] - unitsArr[i]*unitMultiplicator;
		}
		insertCallbackUnit( unitInputs[i], callbackNum );
	}
}

if (doc.URL.indexOf('screen=place') != -1) {
	collectUnitsThere();
	collectUnits();
	collectUnitInputs();
	
	if (doc.URL.indexOf('try=back') != -1) {
		insertCallbackUnits();
	} else {
		if (fillTheCoords) {
			fillCoords();
		}
		insertSentUnits();
	}
} else if ((doc.URL.indexOf('screen=train') != -1) && 
	(doc.URL.indexOf('mode=mass') == -1)) 
{
	collectUnits();
	collectUnitInputs();
	insertSentUnits();
} else {
	if (showAlert) {
		alert("Ez a script csak a gy�lekez�helyen m�k�dik.\nUgr�s...");
	}
	self.location=win.game_data.link_base_pure.replace(/screen\=\w*/i,"screen=place")
}

end(0);
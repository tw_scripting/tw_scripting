var theTable;
var theDoc;
var returningAttackPrefix = "Visszaérkezés";
var returningAttackSuffix = "faluból";
var outputID='id_date_output';
var formLinkStart="https://docs.google.com/forms/d/14m_zvQPAvAoWe4hk_cy9QgBeiBNiA949_WOh3B-sDyA/formResponse?hl=hu_HU&ifq&entry.202448953=";
var formLinkEnd="&submit=Submit";
var enabledServers=["hu22"];

function myStrIsReturningAttack(str) {
	var result =
		(str.indexOf(returningAttackPrefix) == 0) //starts with prefix
		&&
		(str.indexOf(returningAttackSuffix, str.length - returningAttackSuffix.length) != -1); //ends with suffix
	return result;
}

function myGetInner(obj){
	return obj.innerHTML;
}

function strToInt(strVal){
	return parseInt(strVal,10);
}

function myZeroPad(strVal){
	var intVal = parseInt(strVal,10);
	return (intVal>9 ? ''+intVal : '0'+intVal);
}
	
function myTWDateStrToDate(strDate){
	//remove MilliSec, if it is there.
	var arrMs = strDate.match(/:(\d{3})$/i);
	if(arrMs){
		strDate = strDate.replace(/:(\d{3})$/i,'');
	}
	var dtNew = new Date(strDate);

	if(dtNew=='Invalid Date'){
		var arrDate = strDate.match(/\b(\d+)\b/ig); //create an array of consecutive numberstrings
		arrDate = arrDate.map(strToInt); //convert these strings to numbers

		if(arrDate[2]<2000){ //year
			arrDate[2] += 2000;
		}
		dtNew = new Date(arrDate[2],arrDate[1]-1,arrDate[0],arrDate[3],arrDate[4],arrDate[5]);
	}
	if(arrMs){
		dtNew.setMilliseconds(arrMs[1]);
	}
	return dtNew;
}

function myDateToString(dtDate){
	var intMs=dtDate.getMilliseconds();
	var result = '' + myZeroPad(dtDate.getHours())+':'+
		myZeroPad(dtDate.getMinutes())+':'+
		myZeroPad(dtDate.getSeconds())+'.'+
		(intMs>99?intMs:'0'+myZeroPad(intMs))+' '+
		myZeroPad(dtDate.getDate())+'/'+myZeroPad(dtDate.getMonth()+1)+'/'+dtDate.getFullYear();
	return result;
}

function myDateToStringHuman(dtDate){
	var intMs=dtDate.getMilliseconds();
	var result = '' + 
		dtDate.getFullYear() + '/' + myZeroPad(dtDate.getMonth()+1) + '/' + myZeroPad(dtDate.getDate()) + ' ' +
		myZeroPad(dtDate.getHours())+':'+
		myZeroPad(dtDate.getMinutes())+':'+
		myZeroPad(dtDate.getSeconds())+'.'+
		(intMs>99?intMs:'0'+myZeroPad(intMs));
	return result;
}

function myTimeStrToSeconds(strVal) {
	var arrTime = strVal.match(/\d+/ig);
	var result = (arrTime[0]*3600 + arrTime[1]*60 + arrTime[2]*1);
	return result;
}

function mySecondsToTimeStr(intVal){
	return ''+myZeroPad(intVal/3600)+':'+myZeroPad(intVal%(3600)/60 )+':'+myZeroPad(intVal%60);
}

function getIdFromUrl(urlStr) {
	//... &id=123456& ...
	//split url so the second element (at index 1) starts with the id
	//split the second part to get the id
	return urlStr.split("&id=")[1].split("&")[0];
}

function execute() {
	var theDoc=document;
	if(window.frames.length>0){
		theDoc=window.main.document;
	}
	url = theDoc.URL;
	
	if (url.indexOf("screen=info_command") == -1) {
		alert("Ez a script csak saját parancsképernyőn működik!\nKattints egy úton levő támadásodra/erősítésedre!");
		return;
	} 	
	if (url.indexOf("type=own") == -1) {
		alert("Ez a script csak általunk küldött támadásra/erősítésre alkalmazható, bejövőkre nem.");
		return;
	}
	var foundEnabledServer = false;
	for (serverIdx in enabledServers) {
		if (url.indexOf(enabledServers[serverIdx]) != -1) {
			foundEnabledServer = true;
			break;
		}
	}
	if (!foundEnabledServer) {
		alert("Ez a script jelenleg csak ezeken a szervereken működik:\n" + enabledServers.toString());
		return;
	}
	
	theTable = theDoc.getElementById('labelText').parentNode.parentNode.parentNode.parentNode.parentNode;
	var arrRows = theTable.rows;
	var numRows = arrRows.length;

	var id = getIdFromUrl(url);
	
	//calculating duration multiplicator
	var durationMultiplicator = 1;
	var parentTable = theTable.parentNode;
	var titleStr = myGetInner(parentTable.getElementsByTagName('h2')[0]);
	if (myStrIsReturningAttack(titleStr)) {
		durationMultiplicator = 2;
		//alert("Visszatérő sikeres támadók: duplázzuk a menetidőt");
	}
	
	//calculating arrival
	var dateHolderObject = arrRows[6].cells[1];
	var dateArrivalStr = 
		typeof(dateHolderObject.innerText)=='undefined' ?
			  dateHolderObject.textContent
			: dateHolderObject.innerText;
	var dateArrival = myTWDateStrToDate(dateArrivalStr); //create date object
	var msecsArrival = dateArrival.getTime(); //msecs since Jan 1, 1970
	var myDateArrivalStr = myDateToString(dateArrival);
	
	//calculating duration
	var strDuration = myGetInner(arrRows[5].cells[1]);
	var msecsDuration = myTimeStrToSeconds(strDuration) * 1000;
	
	//calculating start time
	var msecsStarted = msecsArrival - msecsDuration * durationMultiplicator;
	var dateStarted = new Date(msecsStarted);
	var myDateStartedStr = myDateToString(dateStarted);
	var myDateStartedHuman = myDateToStringHuman(dateStarted);
	
	var outputStr = "" + id + "|" + myDateStartedStr;
	var encodedOutputStr = encodeURIComponent(outputStr);
	var outputStrHuman = "id: " + id + " ekkor: " + myDateStartedHuman;
	
	//row for human readable output:
	var newRow = theTable.insertRow(0);
	var outputCell = newRow.insertCell(0);
	outputCell.colSpan = 5;
	var input = theDoc.createElement("input");
	outputCell.appendChild(input);
	input.type = "text";
	input.size = "60";
	input.value = outputStrHuman;
	input.readOnly=true;
	input.select();
	
	//row for form link:
	var formFilledUrl = formLinkStart + encodedOutputStr + formLinkEnd;
	newRow = theTable.insertRow(1);
	outputCell = newRow.insertCell(0);
	outputCell.colSpan = 5;
	frameForm = theDoc.createElement("iframe");
	outputCell.appendChild(frameForm);
	frameForm.setAttribute("src", formFilledUrl);
	frameForm.style.width = 500+"px";
	frameForm.style.height = 220+"px";
}

execute();
void(0);
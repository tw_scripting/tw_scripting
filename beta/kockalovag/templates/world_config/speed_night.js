/*
 *  @author: kockalovag (kockalovag@gmail.com)
 *  [Script] - utilities for retrieving world speed and distances
 */

//TW
var Timing;

function fnGetConfig(){
	var oRequest = new XMLHttpRequest();
	var sURL = "https://"+window.location.hostname + "/interface.php?func=get_config";
	oRequest.open("GET",sURL,0); oRequest.send(null);
	if(oRequest.status==200){
		return oRequest.responseXML;
	} else {
		alert("Error executing XMLHttpRequest call to get Config!");
	}
}

var minutesPerField = {
    spear :    18, 
    sword :    22,
    axe :      18, 
    archer :   18, 
    spy :       9, 
    light :    10, 
    marcher :  10,
    heavy :    11,
    ram :      30, 
    catapult : 30,
    knight :   10, 
    snob :     35 
};

function zeroPad(intVal) {
    return intVal > 9 ? '' + intVal : '0' + intVal;
}

function getTimeLength(unit, distance) {
    var secondsDiff = Math.round(distance * minutesPerField[unit] * 60 / theWorldSpeed / theUnitSpeed);
    var hours = Math.floor(secondsDiff/3600);
    var minutes = Math.floor(secondsDiff%3600/60);
    var seconds = Math.floor(secondsDiff%60);
    result = {
        seconds : seconds,
        minutes : minutes,
        hours : hours,
        whole_days : Math.floor(hours/24),
        remaining_hours : hours%24,
        seconds_length : secondsDiff
    };
    return result;
}

function getArrivalTimeStr(startDateTime, myLength) {
    var secondsDiff = myLength.seconds_length % 86400; //a day is 24 hours * 60 minutes * 60 = 86400 seconds
    var arrivalDateTime = new Date(startDateTime.getTime() + secondsDiff*1000); //adding millisecs
    //CAUTION! To produce the time (which is needed now) the above method is sufficient. But the date part might be incorrect!
    //http://stackoverflow.com/questions/1197928/how-to-add-30-minutes-to-a-javascript-date-object
    var myArrivalTime = {
        seconds : arrivalDateTime.getSeconds(),
        minutes : arrivalDateTime.getMinutes(),
        hours : arrivalDateTime.getHours(),
        whole_days : 0,
        remaining_hours : arrivalDateTime.getHours()
    };
    var timeStr = getTimeStr(myArrivalTime, false);
    if (theNightSettings.isActive) {
        if (theNightSettings.startHour <= myArrivalTime.hours 
            && myArrivalTime.hours < theNightSettings.endHour)
        {
            timeStr = '<font color="red">' + timeStr + '</font>';
        }
    }
    return timeStr;
}

function getCurrentServerDateTime() {
    return new Date(Timing.getCurrentServerTime());
}

function getCoordFromStr(coordStr) {
    var coordMatched = coordStr.match(/\d{3}\|\d{3}/)[0];
    var coordsSplit = coordMatched.split("|");
    var coord = {
        x : parseInt(coordsSplit[0]),
        y : parseInt(coordsSplit[1])
    };
    return coord;
}

function getDistance(oneCoord, targetCoord) {
    return Math.sqrt(
        Math.pow(oneCoord.x - targetCoord.x, 2) + 
        Math.pow(oneCoord.y - targetCoord.y, 2));
}

function setupParams() {
    var xmlDoc = fnGetConfig();
    theUnitSpeed = xmlDoc.getElementsByTagName('unit_speed')[0].childNodes[0].nodeValue;
    theWorldSpeed = xmlDoc.getElementsByTagName('speed')[0].childNodes[0].nodeValue; 
    var nightNode = xmlDoc.getElementsByTagName('night')[0];
    theNightSettings = {};
    theNightSettings.isActive = nightNode.getElementsByTagName('active')[0].childNodes[0].nodeValue == 1;
    if (theNightSettings.isActive) {
        theNightSettings.startHour = parseInt(nightNode.getElementsByTagName('start_hour')[0].childNodes[0].nodeValue, 10);
        theNightSettings.endHour = parseInt(nightNode.getElementsByTagName('end_hour')[0].childNodes[0].nodeValue, 10);
    }
    
    tableMgr.theVillageCoord = { 
        x : game_data.village.x,
        y : game_data.village.y
    };
}

void(0);
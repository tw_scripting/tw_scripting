versioningLoaded = true;

function versionCompare(v1, v2) {
	var v1arr = v1.split(".");
	var v2arr = v2.split(".");
	v1arr = v1arr.map( function(e) { 
		return parseInt(e, 10);
	});
	v2arr = v2arr.map( function(e) { 
		return parseInt(e, 10);
	});
	var i = 0;
	while (i < v1arr.length && i < v2arr.length && v1arr[i] == v2arr[i]) {
		++i;
	}
	if (i >= v1arr.length) {
		if (i >= v2arr.length) { //v1 finished, v2 finished, equal
			return 0;
		} else { //v1 finished, v2 not finished, v2 is bigger 
			return 1;
		}
	} else {
		if (i >= v2arr.length) { //v1 not finished, v2 finished, v1 is bigger
			return -1;		
		} else { //v1 not finished, v2 not finished, current element decides
			if (v1arr[i] < v2arr[i]) {
				return 1;
			} else {
				return -1;
			}
		}
	}
}
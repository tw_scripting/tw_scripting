function ExternalMethods() {
	//Négyzetfüggvény
	this.sq = function(x) {
		return x * x;
	};

	//Távolság kiszámítása
	this.distance = function(p1, p2) {
		return Math.sqrt(Math.abs((this.sq(p2.x - p1.x) + this.sq(p2.y - p1.y)))).toFixed(1);
	};

	//Aktuális falu koordinátája
	this.getCurrentCoord = function() {
		var text  = $('#menu_row2').children('td').has('b.nowrap').text().split(" ");
		var coord = text[0].replace("(", "").replace(")", "").split("|");

		return {
			x: parseInt(coord[0], 10),
			y: parseInt(coord[1], 10)
		};
	};
}

function CombinedSorter() {
	var mainExternal = new ExternalMethods();
	var mainChurch   = false;
	var mainArcher   = false;
	var mainKnight   = false;
	var mainMilitia  = false;
	var mainPosition = {};
	var mainArray    = [];
	
	//Beállítások begyűjtése
	function checkSettings() {
		$('#combined_table').find('tr').first().children('th').last().after('<th id="script_distance">Távolság</th>');

		var tr = $('#combined_table').find('tr').first();
		var children = tr.children('th');

		if(children.is(':has(img[src*="church.png"])')) {
			mainChurch          = true;
			mainPosition.church = children.index(tr.find('th:has(img[src*="church.png"])'));
		}

		if(children.is(':has(img[src*="unit_archer.png"])') && children.is('th:has(img[src*="unit_marcher.png"])')) {
			mainArcher           = true;
			mainPosition.archer  = children.index(tr.find('th:has(img[src*="unit_archer.png"])'));
			mainPosition.marcher = children.index(tr.find('th:has(img[src*="unit_marcher.png"])'));
		}

		if(children.is(':has(img[src*="unit_knight.png"])')) {
			mainKnight          = true;
			mainPosition.knight = children.index(tr.find('th:has(img[src*="unit_knight.png"])'));
		}

		if(children.is(':has(img[src*="unit_militia.png"])')) {
			mainMilitia          = true;
			mainPosition.militia = children.index(tr.find('th:has(img[src*="unit_militia.png"])'));
		}

		mainPosition.village  = 1;
		mainPosition.farm     = children.index(tr.find('th:has(img[src*="farm.png"])'));
		mainPosition.spear    = children.index(tr.find('th:has(img[src*="unit_spear.png"])'));
		mainPosition.sword    = children.index(tr.find('th:has(img[src*="unit_sword.png"])'));
		mainPosition.axe      = children.index(tr.find('th:has(img[src*="unit_axe.png"])'));
		mainPosition.spy      = children.index(tr.find('th:has(img[src*="unit_spy.png"])'));
		mainPosition.light    = children.index(tr.find('th:has(img[src*="unit_light.png"])'));
		mainPosition.heavy    = children.index(tr.find('th:has(img[src*="unit_heavy.png"])'));
		mainPosition.ram      = children.index(tr.find('th:has(img[src*="unit_ram.png"])'));
		mainPosition.catapult = children.index(tr.find('th:has(img[src*="unit_catapult.png"])'));
		mainPosition.snob     = children.index(tr.find('th:has(img[src*="unit_snob.png"])'));
		mainPosition.trader   = children.index(tr.find('th:has(img[src*="trader.png"])'));
		mainPosition.distance = children.index(children.last());
	}
	
	//Adatok összeszedése
	function collectData() {
		var td           = [];
		var name         = [];
		var farm         = [];
		var trader       = [];
		var continent    = [];
		var coord        = [];
		var currentCoord = mainExternal.getCurrentCoord();
		var distance     = 0;
		
		$('table#combined_table').find('tr').not(':first').each(function(key, val) {
			td        = $(val).children('td');
			name      = td.eq(mainPosition.village).find('a').find('span').first().text().split(" ");
			farm      = td.eq(mainPosition.farm).text().replace("(", "").replace(")", "").split(" ");
			trader    = td.eq(mainPosition.trader).text().split("/");
			continent = name[name.length - 1].replace("K", "");
			coord     = name[name.length - 2].replace("(", "").replace(")", "").split("|");
			distance  = mainExternal.distance(currentCoord, {
				x: coord[0],
				y: coord[1]
			});
			
			name.splice(name.length - 2, 2);
			
			td.last().after('<td>' + distance + '</td>');
			
			mainArray.push({
				continent  : parseInt(continent, 10),
				x          : parseInt(coord[0], 10),
				y          : parseInt(coord[1], 10),
				distance   : distance,
				name       : name.join(" "),
				freePop    : parseInt(farm[0], 10),
				farm       : parseInt(farm[1], 10),
				freeTrader : parseInt(trader[0], 10),
				allTrader  : parseInt(trader[1], 10),
				spear      : parseInt(td.eq(mainPosition.spear).text(), 10),
				sword      : parseInt(td.eq(mainPosition.sword).text(), 10),
				axe        : parseInt(td.eq(mainPosition.axe).text(), 10),
				spy        : parseInt(td.eq(mainPosition.spy).text(), 10),
				light      : parseInt(td.eq(mainPosition.light).text(), 10),
				heavy      : parseInt(td.eq(mainPosition.heavy).text(), 10),
				ram        : parseInt(td.eq(mainPosition.ram).text(), 10),
				catapult   : parseInt(td.eq(mainPosition.catapult).text(), 10),
				snob       : parseInt(td.eq(mainPosition.snob).text(), 10),
				tr         : $(val).html()
			});
			
			if(mainArcher) {
				mainArray[key].archer  = parseInt(td.eq(mainPosition.archer).text(), 10);
				mainArray[key].marcher = parseInt(td.eq(mainPosition.marcher).text(), 10);
			}
			
			if(mainKnight) {
				mainArray[key].knight = parseInt(td.eq(mainPosition.knight).text(), 10);
			}
			
			if(mainMilitia) {
				mainArray[key].militia = parseInt(td.eq(mainPosition.militia).text(), 10)
			}
			
			if(mainChurch) {
				if(td.eq(mainPosition.church).is(':has(img[src*="prod_running.png"])') || td.eq(mainPosition.church).is('span.running')) {
					mainArray[key].church = 1;
				} else {
					mainArray[key].church = 0;
				}
			}
		});
	}
	
	//Táblázat újraírása
	function rewriteTable() {
		var rows = document.getElementById('combined_table').rows;

        for(var i = 0; i < mainArray.length; i++) {
			rows[i+1].innerHTML = mainArray[i].tr;
		}
	}
	
	//Sorbarendezés
	function sorter(order, index) {
		if(index == "name") {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					if(a.name.toLowerCase() == b.name.toLowerCase()) {
						return 0;
					} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					if(a.name.toLowerCase() == b.name.toLowerCase()) {
						return 0;
					} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} else {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					return a[index] - b[index];
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					return a[index] - b[index];
				});
			}
		}
		
		rewriteTable();
	}
	
	//Táblázat bindelése
	function bind() {
		var th      = $('#combined_table').find('tr').first().children('th');
		var pattern = '<div class="sort" id="{INDEX}"><div class="asc"></div><div class="desc"></div></div>';
		
		th.eq(mainPosition.village).find('a').after(pattern.replace("{INDEX}", "name") + '<div class="text">(</div>' + pattern.replace("{INDEX}", "x") + '<div class="text">|</div>' + pattern.replace("{INDEX}", "y") + '<div class="text">) K</div>' + pattern.replace("{INDEX}", "continent"));
		th.eq(mainPosition.farm).find('a').after(pattern.replace("{INDEX}", "freePop") + '<div class="text">(</div>' + pattern.replace("{INDEX}", "farm") + '<div class="text">)</div>');
		th.eq(mainPosition.spear).find('a').after(pattern.replace("{INDEX}", "spear"));
		th.eq(mainPosition.sword).find('a').after(pattern.replace("{INDEX}", "sword"));
		th.eq(mainPosition.axe).find('a').after(pattern.replace("{INDEX}", "axe"));
		th.eq(mainPosition.spy).find('a').after(pattern.replace("{INDEX}", "spy"));
		th.eq(mainPosition.light).find('a').after(pattern.replace("{INDEX}", "light"));
		th.eq(mainPosition.heavy).find('a').after(pattern.replace("{INDEX}", "heavy"));
		th.eq(mainPosition.ram).find('a').after(pattern.replace("{INDEX}", "ram"));
		th.eq(mainPosition.catapult).find('a').after(pattern.replace("{INDEX}", "catapult"));
		th.eq(mainPosition.snob).find('a').after(pattern.replace("{INDEX}", "snob"));
		th.eq(mainPosition.trader).find('a').after(pattern.replace("{INDEX}", "freeTrader") + '<div class="text">/</div>' + pattern.replace("{INDEX}", "allTrader"));
		th.eq(mainPosition.distance).append(pattern.replace("{INDEX}", "distance"));
		
		if(mainArcher) {
			th.eq(mainPosition.archer).find('a').after(pattern.replace("{INDEX}", "archer"));
			th.eq(mainPosition.marcher).find('a').after(pattern.replace("{INDEX}", "marcher"));
		}
		
		if(mainKnight) {
			th.eq(mainPosition.knight).find('a').after(pattern.replace("{INDEX}", "knight"));
		}
		
		if(mainMilitia) {
			th.eq(mainPosition.militia).find('img').after(pattern.replace("{INDEX}", "militia"));
		}
		
		if(mainChurch) {
			th.eq(mainPosition.church).find('img').after(pattern.replace("{INDEX}", "church"));
		}
		
		$('div.text').css('float', 'left');
		$('div.sort').css({
			'float': 'left',
			'width': '0.5em'
		});
		$('div.sort div.asc').css({
			'width': '0',
			'height': '0',
			'line-height': '0',
			'margin-top': '.0em',
			'border-top': '0px solid',
			'border-right': '.3em solid rgb(222,211,185)',
			'border-bottom': '.8em solid',
			'border-left': '.3em solid rgb(222,211,185)',
			'float': 'left',
			'margin-left': '0px',
			'border-top-color': '#fff',
			'border-bottom-color': '#fff'
		});
		$('div.sort div.desc').css({
			'width': '0',
			'height': '0',
			'margin-top': '.2em',
			'line-height': '0',
			'border-top': '.8em solid ',
			'border-left': '.3em solid rgb(222,211,185)',
			'border-right': '.3em solid rgb(222,211,185)',
			'border-bottom': '0px solid',
			'float': 'left',
			'border-top-color': '#fff',
			'border-bottom-color': '#fff'
		});
		$('div.sort div').hover(function() {
			if(!$(this).is('.active_sorter')) {
				$(this).css({
					'border-top-color': '#0082BE',
					'border-bottom-color': '#0082BE'
				});
			}
		}, function() {
			if(!$(this).is('.active_sorter')) {
				$(this).css({
					'border-top-color': '#fff',
					'border-bottom-color': '#fff'
				});
			}
		}).on('click', function() {
			$('div.active_sorter').css({
				'border-top-color': '#fff',
				'border-bottom-color': '#fff'
			}).removeClass('active_sorter');
			
			$(this).addClass("active_sorter").css({
				'border-top-color': '#C00',
				'border-bottom-color': '#C00'
			});
			
			sorter($(this).attr('class').replace(' active_sorter', ''), $(this).parent().attr('id'));
		});
	}

	this.run = function() {
		checkSettings();
		collectData();
		bind();
	};
}
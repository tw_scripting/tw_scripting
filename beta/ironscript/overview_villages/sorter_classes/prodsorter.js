function ExternalMethods() {
	//Négyzetfüggvény
	this.sq = function(x) {
		return x * x;
	};

	//Távolság kiszámítása
	this.distance = function(p1, p2) {
		return Math.sqrt(Math.abs((this.sq(p2.x - p1.x) + this.sq(p2.y - p1.y)))).toFixed(1);
	};

	//Aktuális falu koordinátája
	this.getCurrentCoord = function() {
		var text  = $('#menu_row2').children('td').has('b.nowrap').text().split(" ");
		var coord = text[0].replace("(", "").replace(")", "").split("|");

		return {
			x: parseInt(coord[0], 10),
			y: parseInt(coord[1], 10)
		};
	};
}

function ProdSorter() {
	var mainExternal = new ExternalMethods();
	var mainPosition = {};
	var mainArray    = [];
	
	//Beállítások begyűjtése
	function checkSettings() {
		$('table#production_table').find('tr').first().children('th').last().after('<th id="script_distance">Távolság</th>');

		mainPosition.village   = 1;
		mainPosition.points    = 2;
		mainPosition.resources = 3;
		mainPosition.store     = 4;
		mainPosition.trader    = 5;
		mainPosition.farm      = 6;
		mainPosition.build     = 7;
		mainPosition.develop   = 8;
		mainPosition.recruit   = 9;
		mainPosition.distance  = 10;
	}
	
	//Adatok összeszedése
	function collectData() {
		var td           = [];
		var name         = [];
		var farm         = [];
		var trader       = [];
		var continent    = [];
		var coord        = [];
		var currentCoord = mainExternal.getCurrentCoord();
		var distance     = 0;
		
		$('table#production_table').find('tr').not(':first').each(function(key, val) {
			td        = $(val).children('td');
			name      = td.eq(mainPosition.village).find('a').find('span').first().text().split(" ");
			farm      = td.eq(mainPosition.farm).text().split("/");
			trader    = td.eq(mainPosition.trader).text().split("/");
			continent = name[name.length - 1].replace("K", "");
			coord     = name[name.length - 2].replace("(", "").replace(")", "").split("|");
			distance  = mainExternal.distance(currentCoord, {
				x: coord[0],
				y: coord[1]
			});
			
			name.splice(name.length - 2, 2);
			
			td.last().after('<td>' + distance + '</td>');
			
			mainArray.push({
				continent  : parseInt(continent, 10),
				x          : parseInt(coord[0], 10),
				y          : parseInt(coord[1], 10),
				distance   : distance,
				name       : name.join(" "),
				points     : parseInt(td.eq(mainPosition.points).text().replace(".", ""), 10),
				maxPop     : parseInt(farm[1], 10),
				population : parseInt(farm[0], 10),
				freeTrader : parseInt(trader[0], 10),
				allTrader  : parseInt(trader[1], 10),
				wood       : parseInt(td.eq(mainPosition.resources).children('span').eq(0).text().replace(".", ""), 10),
				stone      : parseInt(td.eq(mainPosition.resources).children('span').eq(1).text().replace(".", ""), 10),
				iron       : parseInt(td.eq(mainPosition.resources).children('span').eq(2).text().replace(".", ""), 10),
				store      : parseInt(td.eq(mainPosition.store).text(), 10),
				build      : td.eq(mainPosition.build).children('ul').children('li').length,
				develop    : td.eq(mainPosition.develop).children('img').length,
				recruit    : td.eq(mainPosition.recruit).children('ul').children('li').length,
				tr         : $(val).html()
			});
		});
	}
	
	//Táblázat újraírása
	function rewriteTable() {
		var rows = document.getElementById('production_table').rows;

        for(var i = 0; i < mainArray.length; i++) {
			rows[i+1].innerHTML = mainArray[i].tr;
		}
	}
	
	//Sorbarendezés
	function sorter(order, index) {
		if(index == "name") {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					if(a.name.toLowerCase() == b.name.toLowerCase()) {
						return 0;
					} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					if(a.name.toLowerCase() == b.name.toLowerCase()) {
						return 0;
					} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} else {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					return a[index] - b[index];
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					return a[index] - b[index];
				});
			}
		}
		
		rewriteTable();
	}
	
	//Táblázat bindelése
	function bind() {
		var th      = $('table#production_table').find('tr').first().children('th');
		var pattern = '<div class="sort" id="{INDEX}"><div class="asc"></div><div class="desc"></div></div>';
		
		th.eq(mainPosition.village).find('a').after('<br>' + pattern.replace("{INDEX}", "name") + '<div class="text">(</div>' + pattern.replace("{INDEX}", "x") + '<div class="text">|</div>' + pattern.replace("{INDEX}", "y") + '<div class="text">) K</div>' + pattern.replace("{INDEX}", "continent"));
		th.eq(mainPosition.points).find('a').after(pattern.replace("{INDEX}", "points"));
		th.eq(mainPosition.resources).append('<br>' + pattern.replace("{INDEX}", "wood") + '<div class="text">&nbsp;</div>' + pattern.replace("{INDEX}", "stone") + '<div class="text">&nbsp;</div>' + pattern.replace("{INDEX}", "iron"));
		th.eq(mainPosition.store).find('a').after(pattern.replace("{INDEX}", "store"));
		th.eq(mainPosition.trader).find('a').after(pattern.replace("{INDEX}", "freeTrader") + '<div class="text">/</div>' + pattern.replace("{INDEX}", "allTrader"));
		th.eq(mainPosition.farm).find('a').after('<br>' + pattern.replace("{INDEX}", "population") + '<div class="text">/</div>' + pattern.replace("{INDEX}", "maxPop"));
		th.eq(mainPosition.build).append('<br>' + pattern.replace("{INDEX}", "build"));
		th.eq(mainPosition.develop).append('<br>' + pattern.replace("{INDEX}", "develop"));
		th.eq(mainPosition.recruit).append('<br>' + pattern.replace("{INDEX}", "recruit"));
		th.eq(mainPosition.distance).append(pattern.replace("{INDEX}", "distance"));
		
		$('div.text').css('float', 'left');
		$('div.sort').css({
			'float': 'left',
			'width': '0.5em'
		});
		$('div.sort div.asc').css({
			'width': '0',
			'height': '0',
			'line-height': '0',
			'margin-top': '.0em',
			'border-top': '0px solid',
			'border-right': '.3em solid rgb(222,211,185)',
			'border-bottom': '.8em solid',
			'border-left': '.3em solid rgb(222,211,185)',
			'float': 'left',
			'margin-left': '0px',
			'border-top-color': '#fff',
			'border-bottom-color': '#fff'
		});
		$('div.sort div.desc').css({
			'width': '0',
			'height': '0',
			'margin-top': '.2em',
			'line-height': '0',
			'border-top': '.8em solid ',
			'border-left': '.3em solid rgb(222,211,185)',
			'border-right': '.3em solid rgb(222,211,185)',
			'border-bottom': '0px solid',
			'float': 'left',
			'border-top-color': '#fff',
			'border-bottom-color': '#fff'
		});
		$('div.sort div').hover(function() {
			if(!$(this).is('.active_sorter')) {
				$(this).css({
					'border-top-color': '#0082BE',
					'border-bottom-color': '#0082BE'
				});
			}
		}, function() {
			if(!$(this).is('.active_sorter')) {
				$(this).css({
					'border-top-color': '#fff',
					'border-bottom-color': '#fff'
				});
			}
		}).on('click', function() {
			$('div.active_sorter').css({
				'border-top-color': '#fff',
				'border-bottom-color': '#fff'
			}).removeClass('active_sorter');
			
			$(this).addClass("active_sorter").css({
				'border-top-color': '#C00',
				'border-bottom-color': '#C00'
			});
			
			sorter($(this).attr('class').replace(' active_sorter', ''), $(this).parent().attr('id'));
		});
	}

	this.run = function() {
		checkSettings();
		collectData();
		bind();
	};
}
function GroupSorter() {
	var mainPosition = {};
	var mainArray    = [];
	
	//Beállítások begyűjtése
	function checkSettings() {
		mainPosition.village = 0;
		mainPosition.count   = 1;
		mainPosition.points  = 2;
		mainPosition.farm    = 3;
	}
	
	//Adatok összeszedése
	function collectData() {
		var td        = [];
		var name      = [];
		var farm      = [];
		var continent = [];
		var coord     = [];
		var distance  = 0;
		
		$('table#group_assign_table').find('tr').not(':first').not(':last').not('[class="nohover"]').each(function(key, val) {
			
			td        = $(val).children('td');
			name      = td.eq(mainPosition.village).find('a').find('span').first().text().split(" ");
			
			farm      = td.eq(mainPosition.farm).text().split("/");
			trader    = td.eq(mainPosition.trader).text().split("/");
			continent = name[name.length - 1].replace("K", "");
			coord     = name[name.length - 2].replace("(", "").replace(")", "").split("|");
			
			name.splice(name.length - 2, 2);
			
			mainArray.push({
				continent  : parseInt(continent, 10),
				x          : parseInt(coord[0], 10),
				y          : parseInt(coord[1], 10),
				distance   : distance,
				name       : name.join(" "),
				count      : parseInt(td.eq(mainPosition.count).text(), 10),
				points     : parseInt(td.eq(mainPosition.points).text().replace(".", ""), 10),
				maxPop     : parseInt(farm[1], 10),
				population : parseInt(farm[0], 10),
				tr         : $(val).html()
			});
		});
		
		$('table#group_assign_table').find('tr[class="nohover"]').appendTo('table#group_assign_table');
	}
	
	//Táblázat újraírása
	function rewriteTable() {
		var rows = document.getElementById('group_assign_table').rows;

        for(var i = 0; i < mainArray.length; i++) {
			rows[i+1].innerHTML = mainArray[i].tr;
		}
	}
	
	//Sorbarendezés
	function sorter(order, index) {
		if(index == "name") {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					if(a.name.toLowerCase() == b.name.toLowerCase()) {
						return 0;
					} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					if(a.name.toLowerCase() == b.name.toLowerCase()) {
						return 0;
					} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} else {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					return a[index] - b[index];
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					return a[index] - b[index];
				});
			}
		}
		
		rewriteTable();
	}
	
	//Táblázat bindelése
	function bind() {
		var th      = $('table#group_assign_table').find('tr').first().children('th');
		var pattern = '<div class="sort" id="{INDEX}"><div class="asc"></div><div class="desc"></div></div>';
		
		th.eq(mainPosition.village).find('a').after('<br>' + pattern.replace("{INDEX}", "name") + '<div class="text">(</div>' + pattern.replace("{INDEX}", "x") + '<div class="text">|</div>' + pattern.replace("{INDEX}", "y") + '<div class="text">) K</div>' + pattern.replace("{INDEX}", "continent"));
		th.eq(mainPosition.count).find('a').after('<br>' + pattern.replace("{INDEX}", "count"));
		th.eq(mainPosition.points).find('a').after('<br>' + pattern.replace("{INDEX}", "points"));
		th.eq(mainPosition.farm).find('a').after('<br>' + pattern.replace("{INDEX}", "population") + '<div class="text">/</div>' + pattern.replace("{INDEX}", "maxPop"));
		
		$('div.text').css('float', 'left');
		$('div.sort').css({
			'float': 'left',
			'width': '0.5em'
		});
		$('div.sort div.asc').css({
			'width': '0',
			'height': '0',
			'line-height': '0',
			'margin-top': '.0em',
			'border-top': '0px solid',
			'border-right': '.3em solid rgb(222,211,185)',
			'border-bottom': '.8em solid',
			'border-left': '.3em solid rgb(222,211,185)',
			'float': 'left',
			'margin-left': '0px',
			'border-top-color': '#fff',
			'border-bottom-color': '#fff'
		});
		$('div.sort div.desc').css({
			'width': '0',
			'height': '0',
			'margin-top': '.2em',
			'line-height': '0',
			'border-top': '.8em solid ',
			'border-left': '.3em solid rgb(222,211,185)',
			'border-right': '.3em solid rgb(222,211,185)',
			'border-bottom': '0px solid',
			'float': 'left',
			'border-top-color': '#fff',
			'border-bottom-color': '#fff'
		});
		$('div.sort div').hover(function() {
			if(!$(this).is('.active_sorter')) {
				$(this).css({
					'border-top-color': '#0082BE',
					'border-bottom-color': '#0082BE'
				});
			}
		}, function() {
			if(!$(this).is('.active_sorter')) {
				$(this).css({
					'border-top-color': '#fff',
					'border-bottom-color': '#fff'
				});
			}
		}).on('click', function() {
			$('div.active_sorter').css({
				'border-top-color': '#fff',
				'border-bottom-color': '#fff'
			}).removeClass('active_sorter');
			
			$(this).addClass("active_sorter").css({
				'border-top-color': '#C00',
				'border-bottom-color': '#C00'
			});
			
			sorter($(this).attr('class').replace(' active_sorter', ''), $(this).parent().attr('id'));
		});
	}

	this.run = function() {
		checkSettings();
		collectData();
		bind();
	};
}
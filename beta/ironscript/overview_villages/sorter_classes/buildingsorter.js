//////////////////////////////////////////////////
// Épületek áttekintés                          //
//////////////////////////////////////////////////
function BuildingsSorter() {
	var mainExternal = new ExternalMethods();
	var mainChurch   = false;
	var mainPosition = {};
	var mainArray    = [];
	
	//Beállítások begyűjtése
	function checkSettings() {
		var tr       = $('table#buildings_table').find('tr').first();
		var children = tr.children('th');

		if(children.is(':has(img[src*="church.png"])')) {
			mainChurch               = true;
			mainPosition.church      = children.index(tr.find('th:has(img[src*="church.png"])').first());
			mainPosition.churchFirst = children.index(tr.find('th:has(img[src*="church.png"])').last());
		}

		mainPosition.village    = 2;
		mainPosition.points     = 3;
		mainPosition.main       = children.index(tr.find('th:has(img[src*="main.png"])'));
		mainPosition.barracks   = children.index(tr.find('th:has(img[src*="barracks.png"])'));
		mainPosition.stable     = children.index(tr.find('th:has(img[src*="stable.png"])'));
		mainPosition.garage     = children.index(tr.find('th:has(img[src*="garage.png"])'));
		mainPosition.snob       = children.index(tr.find('th:has(img[src*="snob.png"])'));
		mainPosition.smith      = children.index(tr.find('th:has(img[src*="smith.png"])'));
		mainPosition.place      = children.index(tr.find('th:has(img[src*="place.png"])'));
		mainPosition.statue     = children.index(tr.find('th:has(img[src*="statue.png"])'));
		mainPosition.market     = children.index(tr.find('th:has(img[src*="market.png"])'));
		mainPosition.wood       = children.index(tr.find('th:has(img[src*="wood.png"])'));
		mainPosition.stone      = children.index(tr.find('th:has(img[src*="stone.png"])'));
		mainPosition.iron       = children.index(tr.find('th:has(img[src*="iron.png"])'));
		mainPosition.farm       = children.index(tr.find('th:has(img[src*="farm.png"])'));
		mainPosition.storage    = children.index(tr.find('th:has(img[src*="storage.png"])'));
		mainPosition.hide       = children.index(tr.find('th:has(img[src*="hide.png"])'));
		mainPosition.wall       = children.index(tr.find('th:has(img[src*="wall.png"])'));
		mainPosition.buildqueue = children.index(children.last());
	}
	
	//Adatok összeszedése
	function collectData() {
		var td           = [];
		var name         = [];
		var coord        = [];
		var continent    = "";
		
		$('table#buildings_table').find('tr').not(':first').each(function(key, val) {
			td        = $(val).children('td');
			name      = td.eq(mainPosition.village).find('a').find('span').first().text().split(" ");
			farm      = td.eq(mainPosition.farm).text().replace("(", "").replace(")", "").split(" ");
			trader    = td.eq(mainPosition.trader).text().split("/");
			continent = name[name.length - 1].replace("K", "");
			coord     = name[name.length - 2].replace("(", "").replace(")", "").split("|");
			
			name.splice(name.length - 2, 2);
			
			mainArray.push({
				continent  : parseInt(continent, 10),
				x          : parseInt(coord[0], 10),
				y          : parseInt(coord[1], 10),
				name       : name.join(" "),
				points     : parseInt(td.eq(mainPosition.points).text(), 10),
				main       : parseInt(td.eq(mainPosition.main).text(), 10),
				barracks   : parseInt(td.eq(mainPosition.barracks).text(), 10),
				stable     : parseInt(td.eq(mainPosition.stable).text(), 10),
				garage     : parseInt(td.eq(mainPosition.garage).text(), 10),
				snob       : parseInt(td.eq(mainPosition.snob).text(), 10),
				smith      : parseInt(td.eq(mainPosition.smith).text(), 10),
				place      : parseInt(td.eq(mainPosition.place).text(), 10),
				statue     : parseInt(td.eq(mainPosition.statue).text(), 10),
				market     : parseInt(td.eq(mainPosition.market).text(), 10),
				wood       : parseInt(td.eq(mainPosition.wood).text(), 10),
				stone      : parseInt(td.eq(mainPosition.stone).text(), 10),
				iron       : parseInt(td.eq(mainPosition.iron).text(), 10),
				farm       : parseInt(td.eq(mainPosition.farm).text(), 10),
				storage    : parseInt(td.eq(mainPosition.storage).text(), 10),
				hide       : parseInt(td.eq(mainPosition.hide).text(), 10),
				wall       : parseInt(td.eq(mainPosition.wall).text(), 10),
				buildqueue : td.eq(mainPosition.buildqueue).find('li').length,
				tr         : $(val).html()
			});
			
			if(mainChurch) {
				mainArray[key].church      = parseInt(td.eq(mainPosition.church).text(), 10);
				mainArray[key].churchFirst = parseInt(td.eq(mainPosition.churchFirst).text(), 10);
			}
		});
	}
	
	//Táblázat újraírása
	function rewriteTable() {
		var rows = document.getElementById('buildings_table').rows;

        for(var i = 0; i < mainArray.length; i++) {
			rows[i+1].innerHTML = mainArray[i].tr;
		}
	}
	
	//Sorbarendezés
	function sorter(order, index) {
		if(index == "name") {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					if(a.name.toLowerCase() == b.name.toLowerCase()) {
						return 0;
					} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					if(a.name.toLowerCase() == b.name.toLowerCase()) {
						return 0;
					} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} else {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					return a[index] - b[index];
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					return a[index] - b[index];
				});
			}
		}
		
		rewriteTable();
	}
	
	//Táblázat bindelése
	function bind() {
		var th = $('table#buildings_table').find('tr').first().children('th');
		
		mainExternal.createHeader('{BR}{ARROW:name}{TEXT:(}{ARROW:x}{TEXT:|}{ARROW:y}{TEXT:)&nbsp;K}{ARROW:continent}', th.eq(mainPosition.village));
		mainExternal.createHeader('{BR}{ARROW:points}', th.eq(mainPosition.points));
		mainExternal.createHeader('{BR}{ARROW:main}', th.eq(mainPosition.main));
		mainExternal.createHeader('{BR}{ARROW:barracks}', th.eq(mainPosition.barracks));
		mainExternal.createHeader('{BR}{ARROW:stable}', th.eq(mainPosition.stable));
		mainExternal.createHeader('{BR}{ARROW:garage}', th.eq(mainPosition.garage));
		mainExternal.createHeader('{BR}{ARROW:snob}', th.eq(mainPosition.snob));
		mainExternal.createHeader('{BR}{ARROW:smith}', th.eq(mainPosition.smith));
		mainExternal.createHeader('{BR}{ARROW:place}', th.eq(mainPosition.place));
		mainExternal.createHeader('{BR}{ARROW:statue}', th.eq(mainPosition.statue));
		mainExternal.createHeader('{BR}{ARROW:market}', th.eq(mainPosition.market));
		mainExternal.createHeader('{BR}{ARROW:wood}', th.eq(mainPosition.wood));
		mainExternal.createHeader('{BR}{ARROW:stone}', th.eq(mainPosition.stone));
		mainExternal.createHeader('{BR}{ARROW:iron}', th.eq(mainPosition.iron));
		mainExternal.createHeader('{BR}{ARROW:farm}', th.eq(mainPosition.farm));
		mainExternal.createHeader('{BR}{ARROW:storage}', th.eq(mainPosition.storage));
		mainExternal.createHeader('{BR}{ARROW:hide}', th.eq(mainPosition.hide));
		mainExternal.createHeader('{BR}{ARROW:wall}', th.eq(mainPosition.wall));
		mainExternal.createHeader('{BR}{ARROW:buildqueue}', th.eq(mainPosition.buildqueue));
		
		if(mainChurch) {
			mainExternal.createHeader('{BR}{ARROW:church}', th.eq(mainPosition.church));
			mainExternal.createHeader('{BR}{ARROW:churchFirst}', th.eq(mainPosition.churchFirst));
		}
		
		mainExternal.addCSSRules().on('click', function() {
			sorter($(this).attr('class').replace(' active_sorter', ''), $(this).parent().attr('id'));
		});
	}

	this.run = function() {
		checkSettings();
		collectData();
		bind();
	};
}
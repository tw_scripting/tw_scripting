function ExternalMethods() {
	//Négyzetfüggvény
	this.sq = function(x) {
		return x * x;
	};

	//Távolság kiszámítása
	this.distance = function(p1, p2) {
		return Math.sqrt(Math.abs((this.sq(p2.x - p1.x) + this.sq(p2.y - p1.y)))).toFixed(1);
	};

	//Aktuális falu koordinátája
	this.getCurrentCoord = function() {
		var text  = $('#menu_row2').children('td').has('b.nowrap').text().split(" ");
		var coord = text[0].replace("(", "").replace(")", "").split("|");

		return {
			x: parseInt(coord[0], 10),
			y: parseInt(coord[1], 10)
		};
	};
}

function IncomingsSorter() {
	var mainExternal = new ExternalMethods();
	var mainPosition = {};
	var mainArray    = [];
	
	//Beállítások begyűjtése
	function checkSettings() {
		$('table#incomings_table').find('tr').first().children('th').first().before('<th id="script_attackid">attackid</th>');

		mainPosition.id        = 0;
		mainPosition.command   = 1;
		mainPosition.target    = 2;
		mainPosition.from      = 3;
		mainPosition.player    = 4;
		mainPosition.date      = 5;
	}
	
	//Adatok összeszedése
	function collectData() {
		var td              = [];
		var target          = [];
		var targetContinent = [];
		var targetCoord     = [];
		var from            = [];
		var fromContinent   = [];
		var fromCoord       = [];
		var attackid        = "";
		var attacker        = "";
		var command         = 0;
		
		$('table#incomings_table').find('tr').not(':first').not(':last').each(function(key, val) {
			$(val).children('td').first().before('<td></td>')
			
			td              = $(val).children('td');
			target          = td.eq(mainPosition.target).find('a').first().text().split(" ");
			targetContinent = target[target.length - 1].replace("K", "");
			targetCoord     = target[target.length - 2].replace("(", "").replace(")", "").split("|");
			from            = td.eq(mainPosition.from).find('a').first().text().split(" ");
			fromContinent   = from[from.length - 1].replace("K", "");
			fromCoord       = from[from.length - 2].replace("(", "").replace(")", "").split("|");
			attackid        = td.eq(mainPosition.command).find('input').eq(1).attr('name').replace("id_", "");
			attacker        = td.eq(mainPosition.player).find('a').first().text();
			command         = td.eq(mainPosition.command).find('a').first().attr('class').replace("-icon", "")
			
			target.splice(target.length - 2, 2);
			from.splice(from.length - 2, 2);
			
			td.first().text(attackid);
			
			mainArray.push({
				targetContinent : parseInt(targetContinent, 10),
				targetX         : parseInt(targetCoord[0], 10),
				targetY         : parseInt(targetCoord[1], 10),
				targetName      : target.join(" "),
				fromContinent   : parseInt(fromContinent, 10),
				fromX           : parseInt(fromCoord[0], 10),
				fromY           : parseInt(fromCoord[1], 10),
				fromName        : from.join(" "),
				attackid        : parseInt(attackid, 10),
				attacker        : attacker,
				command         : (command == "support" ? 1 : 0),
				date            : td.eq(mainPosition.date).text(),
				tr              : $(val).html()
			});
		});
	}
	
	//Táblázat újraírása
	function rewriteTable() {
		var rows = document.getElementById('incomings_table').rows;

        for(var i = 0; i < mainArray.length; i++) {
			rows[i+1].innerHTML = mainArray[i].tr;
		}
	}
	
	//Sorbarendezés
	function sorter(order, index) {
		if(index == "targetName") {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					if(a.targetName.toLowerCase() == b.targetName.toLowerCase()) {
						return 0;
					} else if(a.targetName.toLowerCase() < b.targetName.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					if(a.targetName.toLowerCase() == b.targetName.toLowerCase()) {
						return 0;
					} else if(a.targetName.toLowerCase() < b.targetName.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} if(index == "fromName") {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					if(a.fromName.toLowerCase() == b.fromName.toLowerCase()) {
						return 0;
					} else if(a.fromName.toLowerCase() < b.fromName.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					if(a.fromName.toLowerCase() == b.fromName.toLowerCase()) {
						return 0;
					} else if(a.fromName.toLowerCase() < b.fromName.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} if(index == "attacker") {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					if(a.attacker.toLowerCase() == b.attacker.toLowerCase()) {
						return 0;
					} else if(a.attacker.toLowerCase() < b.attacker.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					if(a.attacker.toLowerCase() == b.attacker.toLowerCase()) {
						return 0;
					} else if(a.attacker.toLowerCase() < b.attacker.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} if(index == "date") {
			if(order == "asc") {
				mainArray.sort(function(b, a) {
					if(a.date.toLowerCase() == b.date.toLowerCase()) {
						return 0;
					} else if(a.date.toLowerCase() < b.date.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(a, b) {
					if(a.date.toLowerCase() == b.date.toLowerCase()) {
						return 0;
					} else if(a.date.toLowerCase() < b.date.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} else {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					return a[index] - b[index];
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					return a[index] - b[index];
				});
			}
		}
		
		rewriteTable();
	}
	
	//Táblázat bindelése
	function bind() {
		var th      = $('table#incomings_table').find('tr').first().children('th');
		var pattern = '<div class="sort" id="{INDEX}"><div class="asc"></div><div class="desc"></div></div>';
		
		th.eq(mainPosition.id).append('<br>' + pattern.replace("{INDEX}", "attackid"));
		th.eq(mainPosition.command).append('<br>' + pattern.replace("{INDEX}", "command"));
		th.eq(mainPosition.target).find('a').after('<br>' + pattern.replace("{INDEX}", "targetName") + '<div class="text">(</div>' + pattern.replace("{INDEX}", "targetX") + '<div class="text">|</div>' + pattern.replace("{INDEX}", "targetY") + '<div class="text">) K</div>' + pattern.replace("{INDEX}", "targetContinent"));
		th.eq(mainPosition.from).find('a').after('<br>' + pattern.replace("{INDEX}", "fromName") + '<div class="text">(</div>' + pattern.replace("{INDEX}", "fromX") + '<div class="text">|</div>' + pattern.replace("{INDEX}", "fromY") + '<div class="text">) K</div>' + pattern.replace("{INDEX}", "fromContinent"));
		th.eq(mainPosition.player).find('a').after('<br>' + pattern.replace("{INDEX}", "attacker"));
		th.eq(mainPosition.date).find('a').after('<br>' + pattern.replace("{INDEX}", "date"));
		
		
		$('div.text').css('float', 'left');
		$('div.sort').css({
			'float': 'left',
			'width': '0.5em'
		});
		$('div.sort div.asc').css({
			'width': '0',
			'height': '0',
			'line-height': '0',
			'margin-top': '.0em',
			'border-top': '0px solid',
			'border-right': '.3em solid rgb(222,211,185)',
			'border-bottom': '.8em solid',
			'border-left': '.3em solid rgb(222,211,185)',
			'float': 'left',
			'margin-left': '0px',
			'border-top-color': '#fff',
			'border-bottom-color': '#fff'
		});
		$('div.sort div.desc').css({
			'width': '0',
			'height': '0',
			'margin-top': '.2em',
			'line-height': '0',
			'border-top': '.8em solid ',
			'border-left': '.3em solid rgb(222,211,185)',
			'border-right': '.3em solid rgb(222,211,185)',
			'border-bottom': '0px solid',
			'float': 'left',
			'border-top-color': '#fff',
			'border-bottom-color': '#fff'
		});
		$('div.sort div').hover(function() {
			if(!$(this).is('.active_sorter')) {
				$(this).css({
					'border-top-color': '#0082BE',
					'border-bottom-color': '#0082BE'
				});
			}
		}, function() {
			if(!$(this).is('.active_sorter')) {
				$(this).css({
					'border-top-color': '#fff',
					'border-bottom-color': '#fff'
				});
			}
		}).on('click', function() {
			$('div.active_sorter').css({
				'border-top-color': '#fff',
				'border-bottom-color': '#fff'
			}).removeClass('active_sorter');
			
			$(this).addClass("active_sorter").css({
				'border-top-color': '#C00',
				'border-bottom-color': '#C00'
			});
			
			sorter($(this).attr('class').replace(' active_sorter', ''), $(this).parent().attr('id'));
		});
	}

	this.run = function() {
		checkSettings();
		collectData();
		bind();
	};
}
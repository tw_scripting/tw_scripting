//////////////////////////////////////////////////
// Parancsok sorbarendez�se                     //
//////////////////////////////////////////////////
function CommandsSorter() {
	var mainExternal = new ExternalMethods();
	var mainArcher   = false;
	var mainKnight   = false;
	var mainPosition = {};
	var mainArray    = [];
	
	//Be�ll�t�sok begy�jt�se
	function checkSettings() {
		var tr       = $('table#commands_table').find('tr').first();
		var children = tr.children('th');

		if(children.is(':has(img[src*="unit_archer.png"])') && children.is('th:has(img[src*="unit_marcher.png"])')) {
			mainArcher           = true;
			mainPosition.archer  = children.index(tr.find('th:has(img[src*="unit_archer.png"])'));
			mainPosition.marcher = children.index(tr.find('th:has(img[src*="unit_marcher.png"])'));
		}

		if(children.is(':has(img[src*="unit_knight.png"])')) {
			mainKnight          = true;
			mainPosition.knight = children.index(tr.find('th:has(img[src*="unit_knight.png"])'));
		}

		mainPosition.command  = 0;
		mainPosition.from     = 1;
		mainPosition.date     = 2;
		mainPosition.spear    = children.index(tr.find('th:has(img[src*="unit_spear.png"])'));
		mainPosition.sword    = children.index(tr.find('th:has(img[src*="unit_sword.png"])'));
		mainPosition.axe      = children.index(tr.find('th:has(img[src*="unit_axe.png"])'));
		mainPosition.spy      = children.index(tr.find('th:has(img[src*="unit_spy.png"])'));
		mainPosition.light    = children.index(tr.find('th:has(img[src*="unit_light.png"])'));
		mainPosition.heavy    = children.index(tr.find('th:has(img[src*="unit_heavy.png"])'));
		mainPosition.ram      = children.index(tr.find('th:has(img[src*="unit_ram.png"])'));
		mainPosition.catapult = children.index(tr.find('th:has(img[src*="unit_catapult.png"])'));
		mainPosition.snob     = children.index(tr.find('th:has(img[src*="unit_snob.png"])'));
	}
	
	//Adatok �sszeszed�se
	function collectData() {
		var td               = [];
		var command          = [];
		var commandData      = [];
		var from             = [];
		var fromData         = [];
		
		$('table#commands_table').find('tr').not(':first').not(':last').each(function(key, val) {
			td               = $(val).children('td');
			command          = td.eq(mainPosition.command).find('a').first().text();
			commandData      = command.match(/\((\d+)\|(\d+)\) K(\d+)/);
			from             = td.eq(mainPosition.from).find('a').first().text();
			fromData         = from.match(/\((\d+)\|(\d+)\) K(\d+)/);
			
			mainArray.push({
				command          : command,
				commandX         : parseInt(commandData[1]),
				commandY         : parseInt(commandData[2]),
				commandContinent : parseInt(commandData[3]),
				from             : from,
				fromX            : parseInt(fromData[1]),
				fromY            : parseInt(fromData[2]),
				fromContinent    : parseInt(fromData[3]),
				spear            : parseInt(td.eq(mainPosition.spear).text()),
				sword            : parseInt(td.eq(mainPosition.sword).text()),
				axe              : parseInt(td.eq(mainPosition.axe).text()),
				spy              : parseInt(td.eq(mainPosition.spy).text()),
				light            : parseInt(td.eq(mainPosition.light).text()),
				heavy            : parseInt(td.eq(mainPosition.heavy).text()),
				ram              : parseInt(td.eq(mainPosition.ram).text()),
				catapult         : parseInt(td.eq(mainPosition.catapult).text()),
				snob             : parseInt(td.eq(mainPosition.snob).text()),
				date             : td.eq(mainPosition.date).text(),
				tr               : $(val).html()
			});
			
			if(mainArcher) {
				mainArray[key].archer  = parseInt(td.eq(mainPosition.archer).text());
				mainArray[key].marcher = parseInt(td.eq(mainPosition.marcher).text());
			}
			
			if(mainKnight) {
				mainArray[key].knight = parseInt(td.eq(mainPosition.knight).text());
			}
		});
	}
	
	//T�bl�zat �jra�r�sa
	function rewriteTable() {
		var rows = document.getElementById('commands_table').rows;

        for(var i = 0; i < mainArray.length; i++) {
			rows[i+1].innerHTML = mainArray[i].tr;
		}
	}
	
	//Sorbarendez�s
	function sorter(order, index) {
		if(index == "command") {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					if(a.command.toLowerCase() == b.command.toLowerCase()) {
						return 0;
					} else if(a.command.toLowerCase() < b.command.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					if(a.command.toLowerCase() == b.command.toLowerCase()) {
						return 0;
					} else if(a.command.toLowerCase() < b.command.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} if(index == "from") {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					if(a.from.toLowerCase() == b.from.toLowerCase()) {
						return 0;
					} else if(a.from.toLowerCase() < b.from.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					if(a.from.toLowerCase() == b.from.toLowerCase()) {
						return 0;
					} else if(a.from.toLowerCase() < b.from.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} if(index == "date") {
			if(order == "asc") {
				mainArray.sort(function(b, a) {
					if(a.date.toLowerCase() == b.date.toLowerCase()) {
						return 0;
					} else if(a.date.toLowerCase() < b.date.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(a, b) {
					if(a.date.toLowerCase() == b.date.toLowerCase()) {
						return 0;
					} else if(a.date.toLowerCase() < b.date.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} else {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					return a[index] - b[index];
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					return a[index] - b[index];
				});
			}
		}
		
		rewriteTable();
	}
	
	//T�bl�zat bindel�se
	function bind() {
		var th = $('table#commands_table').find('tr').first().children('th');
		
		mainExternal.createHeader('{BR}{ARROW:command}{TEXT:(}{ARROW:commandX}{TEXT:|}{ARROW:commandY}{TEXT:)&nbsp;K}{ARROW:commandContinent}', th.eq(mainPosition.command));
		mainExternal.createHeader('{BR}{ARROW:from}{TEXT:(}{ARROW:fromX}{TEXT:|}{ARROW:fromY}{TEXT:)&nbsp;K}{ARROW:fromContinent}', th.eq(mainPosition.from));
		mainExternal.createHeader('{BR}{ARROW:date}', th.eq(mainPosition.date));
		mainExternal.createHeader('{BR}{ARROW:spear}', th.eq(mainPosition.spear));
		mainExternal.createHeader('{BR}{ARROW:sword}', th.eq(mainPosition.sword));
		mainExternal.createHeader('{BR}{ARROW:axe}', th.eq(mainPosition.axe));
		mainExternal.createHeader('{BR}{ARROW:spy}', th.eq(mainPosition.spy));
		mainExternal.createHeader('{BR}{ARROW:light}', th.eq(mainPosition.light));
		mainExternal.createHeader('{BR}{ARROW:heavy}', th.eq(mainPosition.heavy));
		mainExternal.createHeader('{BR}{ARROW:ram}', th.eq(mainPosition.ram));
		mainExternal.createHeader('{BR}{ARROW:catapult}', th.eq(mainPosition.catapult));
		mainExternal.createHeader('{BR}{ARROW:snob}', th.eq(mainPosition.snob));
		
		if(mainArcher) {
			mainExternal.createHeader('{BR}{ARROW:archer}', th.eq(mainPosition.archer));
			mainExternal.createHeader('{BR}{ARROW:marcher}', th.eq(mainPosition.marcher));
		}
		
		if(mainKnight) {
			mainExternal.createHeader('{BR}{ARROW:knight}', th.eq(mainPosition.knight));
		}
		
		mainExternal.addCSSRules().on('click', function() {
			sorter($(this).attr('class').replace(' active_sorter', ''), $(this).parent().attr('id'));
		});
	}

	this.run = function() {
		checkSettings();
		collectData();
		bind();
	};
}
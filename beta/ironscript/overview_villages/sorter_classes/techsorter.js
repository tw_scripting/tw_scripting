//////////////////////////////////////////////////
// Fejlesztések szerinti sorbarendezés          //
//////////////////////////////////////////////////
function GroupSorter() {
	var mainExternal = new ExternalMethods();
	var mainChurch   = false;
	var mainArcher   = false;
	var mainKnight   = false;
	var mainMilitia  = false;
	var mainPosition = {};
	var mainArray    = [];
	
	//Beállítások begyűjtése
	function checkSettings() {
		var tr = $('table#techs_table').find('tr').first();
		var children = tr.children('th');

		if(children.is(':has(img[src*="unit_archer.png"])') && children.is('th:has(img[src*="unit_marcher.png"])')) {
			mainArcher           = true;
			mainPosition.archer  = children.index(tr.find('th:has(img[src*="unit_archer.png"])'));
			mainPosition.marcher = children.index(tr.find('th:has(img[src*="unit_marcher.png"])'));
		}
		
		mainPosition.village  = 1;
		mainPosition.spear    = children.index(tr.find('th:has(img[src*="unit_spear.png"])'));
		mainPosition.sword    = children.index(tr.find('th:has(img[src*="unit_sword.png"])'));
		mainPosition.axe      = children.index(tr.find('th:has(img[src*="unit_axe.png"])'));
		mainPosition.spy      = children.index(tr.find('th:has(img[src*="unit_spy.png"])'));
		mainPosition.light    = children.index(tr.find('th:has(img[src*="unit_light.png"])'));
		mainPosition.heavy    = children.index(tr.find('th:has(img[src*="unit_heavy.png"])'));
		mainPosition.ram      = children.index(tr.find('th:has(img[src*="unit_ram.png"])'));
		mainPosition.catapult = children.index(tr.find('th:has(img[src*="unit_catapult.png"])'));
	}
	
	//Adatok összeszedése
	function collectData() {
		var td        = [];
		var name      = [];
		var continent = [];
		var coord     = [];
		
		$('table#techs_table').find('tr').not(':first').each(function(key, val) {
			
			td        = $(val).children('td');
			name      = td.eq(mainPosition.village).find('a').find('span').first().text().split(" ");
			
			farm      = td.eq(mainPosition.farm).text().split("/");
			trader    = td.eq(mainPosition.trader).text().split("/");
			continent = name[name.length - 1].replace("K", "");
			coord     = name[name.length - 2].replace("(", "").replace(")", "").split("|");
			
			name.splice(name.length - 2, 2);
			
			mainArray.push({
				continent  : parseInt(continent, 10),
				x          : parseInt(coord[0], 10),
				y          : parseInt(coord[1], 10),
				distance   : distance,
				name       : name.join(" "),
				
				
				spear      : td.eq(mainPosition.spear).is('span.green') ? 1 : 0,
				sword      : td.eq(mainPosition.sword).is('span.green') ? 1 : 0,
				axe        : td.eq(mainPosition.axe).is('span.green') ? 1 : 0,
				spy        : td.eq(mainPosition.spy).is('span.green') ? 1 : 0,
				light      : td.eq(mainPosition.light).is('span.green') ? 1 : 0,
				heavy      : td.eq(mainPosition.heavy).is('span.green') ? 1 : 0,
				ram        : td.eq(mainPosition.ram).is('span.green') ? 1 : 0,
				catapult   : td.eq(mainPosition.catapult).is('span.green') ? 1 : 0,
				snob       : td.eq(mainPosition.snob).is('span.green') ? 1 : 0,
				tr         : $(val).html()
			});
			
			if(mainArcher) {
				mainArray[key].archer  = td.eq(mainPosition.archer).is('span.green') ? 1 : 0,
				mainArray[key].marcher = td.eq(mainPosition.marcher).is('span.green') ? 1 : 0,
			}
		});
	}
	
	//Táblázat újraírása
	function rewriteTable() {
		var rows = document.getElementById('techs_table').rows;

        for(var i = 0; i < mainArray.length; i++) {
			rows[i+1].innerHTML = mainArray[i].tr;
		}
	}
	
	//Sorbarendezés
	function sorter(order, index) {
		if(index == "name") {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					if(a.name.toLowerCase() == b.name.toLowerCase()) {
						return 0;
					} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					if(a.name.toLowerCase() == b.name.toLowerCase()) {
						return 0;
					} else if(a.name.toLowerCase() < b.name.toLowerCase()) {
						return -1;
					} else {
						return 1;
					}
				});
			}
		} else {
			if(order == "asc") {
				mainArray.sort(function(a, b) {
					return a[index] - b[index];
				});
			} else if(order == "desc") {
				mainArray.sort(function(b, a) {
					return a[index] - b[index];
				});
			}
		}
		
		rewriteTable();
	}
	
	//Táblázat bindelése
	function bind() {
		var th = $('table#techs_table').find('tr').first().children('th');
		
		mainExternal.createHeader('{BR}{ARROW:name}{TEXT:(}{ARROW:x}{TEXT:|}{ARROW:y}{TEXT:)&nbsp;K}{ARROW:continent}', th.eq(mainPosition.village));
		mainExternal.createHeader('{BR}{ARROW:points}', th.eq(mainPosition.points));
		mainExternal.createHeader('{BR}{ARROW:spear}', th.eq(mainPosition.spear));
		mainExternal.createHeader('{BR}{ARROW:sword}', th.eq(mainPosition.sword));
		mainExternal.createHeader('{BR}{ARROW:axe}', th.eq(mainPosition.axe));
		mainExternal.createHeader('{BR}{ARROW:spy}', th.eq(mainPosition.spy));
		mainExternal.createHeader('{BR}{ARROW:light}', th.eq(mainPosition.light));
		mainExternal.createHeader('{BR}{ARROW:heavy}', th.eq(mainPosition.heavy));
		mainExternal.createHeader('{BR}{ARROW:ram}', th.eq(mainPosition.ram));
		mainExternal.createHeader('{BR}{ARROW:catapult}', th.eq(mainPosition.catapult));
		
		if(mainArcher) {
			mainExternal.createHeader('{BR}{ARROW:archer}', th.eq(mainPosition.archer));
			mainExternal.createHeader('{BR}{ARROW:marcher}', th.eq(mainPosition.marcher));
		}
		
		mainExternal.addCSSRules().on('click', function() {
			sorter($(this).attr('class').replace(' active_sorter', ''), $(this).parent().attr('id'));
		});
	}

	this.run = function() {
		checkSettings();
		collectData();
		bind();
	};
}
Accountmanager = {buildings: {},techs: {},valid: true,queue_type: 'building',initQueue: function(type) {
        if (typeof type == 'string')
            Accountmanager.queue_type = type;
        $('#template_queue').find('.bqremove').click(Accountmanager.removeQueue);
        $('#template_queue').disableSelection();
        $('#template_queue').sortable({axis: 'y',handle: '.bqhandle',start: function(event, ui) {
                ui.item.addClass('selected')
            },stop: function(event, ui) {
                ui.item.removeClass('selected');
                Accountmanager.recalcQueue()
            }});
        $.each($('#' + Accountmanager.queue_type + '_summary').children().not('.total'), function(key, value) {
            value = $(value);
            var id = value.data(Accountmanager.queue_type);
            Accountmanager.levelStorage()[id] = parseInt(value.find('a').text())
        });
        Accountmanager.recalcQueue()
    },levelStorage: function() {
        if (Accountmanager.queue_type == 'building') {
            return Accountmanager.buildings
        } else
            return Accountmanager.techs
    },recalcQueue: function() {
        var i;
        for (i in Accountmanager.levelStorage())
            if (Accountmanager.levelStorage().hasOwnProperty(i))
                Accountmanager.levelStorage()[i] = 0;
        Accountmanager.valid = true;
        var pop = 0, points = 0;
        $.each($('#template_queue').children(), function(key, value) {
            value = $(value);
            var id = value.data(Accountmanager.queue_type), levels = parseInt(value.find('.level_relative').text());
            Accountmanager.levelStorage()[id] += levels;
            if (Accountmanager.queue_type == 'building') {
                if (Accountmanager.hasRequ(id) && Accountmanager.buildings[id] <= max_levels[id]) {
                    value.removeClass('error')
                } else {
                    value.addClass('error');
                    Accountmanager.valid = false
                }
                ;
                var before, after;
                if (key == 0) {
                    before = 0;
                    after = levels
                } else {
                    after = Accountmanager.levelStorage()[id];
                    before = after - levels
                }
                ;
                for (var i = before + 1; i <= after; i++)
                    if (building_data[id].hasOwnProperty(i)) {
                        pop += building_data[id][i].pop;
                        points += building_data[id][i].points
                    }
                ;
                value.find('.pop').text(pop);
                value.find('.points').text('%1 pont'.replace('%1', points));
                if (current[id] >= after) {
                    value.find('img').show()
                } else
                    value.find('img').hide()
            }
            ;
            value.find('.level_absolute').text(' (Szint ' + Accountmanager.levelStorage()[id] + ')')
        });
        Accountmanager.updateSummary()
    },hasRequ: function(bid) {
        if (!buildings[bid])
            return true;
        var requ;
        for (requ in buildings[bid])
            if (buildings[bid].hasOwnProperty(requ) && buildings[bid][requ] > Accountmanager.buildings[requ])
                return false;
        return true
    },saveQueue: function() {
        if (!Accountmanager.valid) {
            UI.ErrorMessage('A ment�s nem lehets�ges, az �p�t�si sorban hiba van!');
            return false
        }
        ;
        var queue = new Array(), i = 0;
        $.each($('#template_queue').children(), function(key, value) {
            value = $(value);
            var id = value.data(Accountmanager.queue_type), levels = parseInt(value.find('.level_relative').text());
            queue[i++] = id + ':' + levels
        });
        $('#queue_data').val(queue.join(';'));
        return true
    },updateSummary: function() {
        var total = 0;
        $.each($('#' + Accountmanager.queue_type + '_summary').children().not('.total'), function(key, value) {
            value = $(value);
            var id = value.data(Accountmanager.queue_type);
            value.find('a').text(Accountmanager.levelStorage()[id]);
            total += Accountmanager.levelStorage()[id]
        });
        if (Accountmanager.queue_type == 'tech')
            $('#' + Accountmanager.queue_type + '_summary').find('.total').text(total + ' / ' + tech_max)
    },addQueueItem: function(id, name, levels, direct) {
        if (isNaN(levels)) {
            UI.ErrorMessage('K�r�nk �rj be egy val�s sz�mot.');
            return false
        }
        ;
        if (Accountmanager.queue_type == 'tech') {
            if (Accountmanager.levelStorage()[id] + levels > techs[id].max) {
                UI.ErrorMessage('Nem lehet tov�bbi szintet betenni, mert az egys�g szintje m�r a maximumon van.');
                return false
            }
            ;
            if (tech_max > 0) {
                var current_count = parseInt($('#' + Accountmanager.queue_type + '_summary').find('.total').text());
                if (current_count >= tech_max) {
                    UI.ErrorMessage('A fejleszt�si szint a maximumon van, nem lehet tov�bbi szinteket hozz�adni.');
                    return
                }
            }
        }
        ;
        var baselink = $('#base_link').val().replace('screen=', 'screen=' + id), row = $('<li>').data(Accountmanager.queue_type, id).addClass('vis_item sortable_row');
        row.append($('<div>').addClass('bqhandle').css('float', 'right '));
        row.append($('<div>').addClass('bqremove').css('float', 'right ').click(Accountmanager.removeQueue));
        var info = $('<div>').css('float', 'left').css('width', '70%');
        if (Accountmanager.queue_type == 'building') {
            info.append($('<a>').addClass('inline-icon ' + Accountmanager.queue_type + '-' + id).attr('href', baselink).text(name))
        } else
            info.append($('<span>').addClass('inline-icon ' + Accountmanager.queue_type + '-' + id).text(name));
        info.append($('<span>').addClass('level_relative').text(' +' + levels));
        info.append($('<span>').addClass('level_absolute').text(' (Szint ' + Accountmanager.levelStorage()[id] + ')'));
        info.append($('<span>').addClass('requ_error').text(' Az el�felt�telek nem teljes�lnek vagy a szint �rv�nytelen!'));
        info.append($('<img src="/graphic/confirm.png" style="width: 14px; height: 14px; display: none" title="Ezt az �p�letet m�r fel�p�tetted a k�v�nt szintre ebben a faluban."/>'));
        row.append(info);
        var additional = $('<div>').css('float', 'left');
        if (Accountmanager.queue_type == 'building') {
            additional.append($('<div style="width: 70px; float: left"><span class="icon header population"> </span> <span class="pop"></span></div>'));
            additional.append($('<span class="points"></span>'))
        }
        ;
        row.append(additional);
        row.append('<br style="clear: both" />');
        $('#template_queue').append(row);
        Accountmanager.recalcQueue();
        if (direct && Accountmanager.valid == false)
            UI.ErrorMessage($('.requ_error').first().text())
    },addQueue: function() {
        var levels = parseInt($('#add_levels').val());
        if (isNaN(levels)) {
            UI.ErrorMessage('K�r�nk �rj be egy val�s sz�mot.');
            return false
        }
        ;
        var id = $('#add_' + Accountmanager.queue_type).val(), name = $('#add_' + Accountmanager.queue_type + ' option:selected').text();
        Accountmanager.addQueueItem(id, name, levels, false)
    },removeQueue: function(event) {
        if (Accountmanager.queue_type == 'tech') {
            var $row = $(this).parent(), id = $row.data('tech'), levels = parseInt($row.find('.level_relative').text());
            if (Accountmanager.levelStorage()[id] - levels < techs[id].min) {
                UI.ErrorMessage('Ezt az elemet nem lehet elt�vol�tani, mert az egys�g fejleszt�si szintje a minimum alatt lenne.');
                return false
            }
        }
        ;
        $(this).parent().remove();
        Accountmanager.recalcQueue()
    },editTroops: function(element) {
        element = $(element);
        element.parent().find('input').attr('checked', true);
        $('#template_name').val(element.parent().find('span').text());
        var row = element.parent().parent();
        $.each(row.find('span'), function(key, value) {
            info = $(value);
            var field = info.data('field');
            if (field)
                document.forms['trooplate'].elements[field].value = info.text()
        });
        Accountmanager.calcPop()
    },applyTrooplate: function(element, check) {
        var option = $(element).find('option:selected'), data = eval(option.data('json'));
        $.each(document.forms["trooplate"].elements, function(key, value) {
            var input = $(value);
            if (input.attr('type') == 'text') {
                var wert = data ? data[input.attr('name')] : '';
                input.val(wert)
            }
        });
        if (check) {
            $('.am_troops_edit').attr('checked', false);
            $('.am_troops_edit[value=' + data.id + ']').attr('checked', true)
        }
        ;
        Accountmanager.calcPop()
    },calcPop: function() {
        var summe = 0;
        $.each($('input[data-pop]'), function(key, value) {
            var input = $(value), anzahl = input.data('pop') * input.val();
            if (anzahl)
                summe += anzahl
        });
        $('#calced_pop').text(summe);
        if (summe > 24e3) {
            $('#calced_pop').addClass('red')
        } else
            $('#calced_pop').removeClass('red')
    },setVillageManagerStatus: function(village_id, link) {
        link = $(link);
        var status = link.data('status');
        if (status == "1") {
            status = 0
        } else if (status == "0") {
            status = 1
        } else
            return true;
        $.get(Accountmanager.change_village_status_link, {status: status,village_id: village_id}, function(data) {
            var img = $('.village_status', link);
            link.data('status', "" + status == "1");
            img.attr('src', img.attr('src').replace(/green|yellow/, status == "1" ? 'yellow' : 'green'))
        });
        return false
    },ignoreWarning: function(village_id, type, link) {
        $.post(Accountmanager.ignore_all_link, {village_id: village_id,type: type}, function(data) {
            $(link).parent('td').parent('tr').remove()
        });
        return false
    },initTooltips: function() {
        UI.ToolTip($('.tooltip'))
    },editTemplateName: function(event_target, template_id) {
        var container = $(event_target).parent(), templat_name = $.trim(container.find('a').text()), edit_input = $('<input type="text" name="name" />').attr('value', templat_name), edit_submit = $('<input type="submit" value="OK" />'), edit_form = $('<form method="post"></form>').attr('action', event_target.href).append(edit_input).append(edit_submit);
        container.children().remove();
        container.append(edit_form);
        return false
    },setWidgetPageSize: function(icon) {
        var page_size = parseInt(prompt("Egy�nileg be�ll�that� oldalm�ret"));
        if (!page_size)
            return false;
        var widget_id = $(icon).parents('.am_widget').data('widget'), data = {widget_id: widget_id,page_size: page_size};
        $.post(Accountmanager.change_pagesize_link, data, function(response) {
            if (response.success)
                location.reload()
        }, 'json');
        return false
    },changeTroops: function() {
        var units = $('#farm_units').serialize();
        $.post(Accountmanager.change_troops_link, units + '&target_screen=' + Accountmanager.target_screen, function(data) {
            if (data.widget) {
                $('#am_widget_Farm .body').html(data.widget);
                UI.ToolTip($('#am_widget_Farm .tooltip'))
            }
        }, 'json')
    },farm: {sendUnits: function(link, target_village, template_id) {
            link = $(link);
            if (link.hasClass('farm_icon_disabled'))
                return false;
            var data = {target: target_village,template_id: template_id,source: game_data.village.id};
            TribalWars.post(Accountmanager.send_units_link, null, data, function(data) {
                $('.farm_village_' + target_village).addClass('farm_icon_disabled');
                Accountmanager.farm.updateOwnUnitsAvailable(data.current_units)
            });
            return false
        },sendUnitsFromReport: function(link, target_village, report_id) {
            link = $(link);
            if (link.hasClass('farm_icon_disabled'))
                return false;
            var data = {report_id: report_id};
            $.post(Accountmanager.send_units_link_from_report, data, function(data) {
                if (data.error) {
                    UI.ErrorMessage(data.error)
                } else {
                    if (typeof data.success === 'string') {
                        UI.SuccessMessage(data.success, 4e3);
                        Accountmanager.farm.updateOwnUnitsAvailable(data.current_units)
                    }
                    ;
                    $('.farm_village_' + target_village).addClass('farm_icon_disabled')
                }
            }, 'json');
            return false
        },updateOwnUnitsAvailable: function(currentUnits) {
            for (unitName in currentUnits)
                if (currentUnits.hasOwnProperty(unitName) && currentUnits[unitName] >= 0) {
                    var unitDisplay = $('#units_home #' + unitName);
                    unitDisplay.text(currentUnits[unitName])
                }
        },deleteReport: function(village_id) {
            $.post(Accountmanager.delete_report_link, {id: village_id}, function(data) {
                if (data.error) {
                    UI.ErrorMessage(data.error)
                } else
                    $('.report_' + village_id).remove()
            }, 'json');
            return false
        },toggleAllVillages: function(el, extended) {
            $.post(Accountmanager.toggle_all_villages_link, {all_villages: 0 + el.checked,extended: 0 + extended,target_screen: Accountmanager.target_screen}, function(data) {
                if (data.widget) {
                    $('#am_widget_Farm .body').html(data.widget);
                    Accountmanager.initTooltips()
                }
            }, 'json')
        },toggleShowFullLosses: function(el, extended) {
            $.post(Accountmanager.toggle_show_full_losses_link, {full_losses: 0 + el.checked,extended: 0 + extended,target_screen: Accountmanager.target_screen}, function(data) {
                if (data.widget) {
                    $('#am_widget_Farm .body').html(data.widget);
                    Accountmanager.initTooltips()
                }
            }, 'json')
        }}}

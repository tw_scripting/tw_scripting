
var win=(window.frames.length>0)?window.main:window;
var coords=[];  
var outputID='villageList';
var encodeID='cbBBEncode';  
var isEncoded=true;

function fnRefresh(){
	win.$('#'+outputID).val(
		coords.map(
	
		function(e){
			return isEncoded?'[coord]'+e+'[\/coord]':e;
		}).join(isEncoded?'\n':' ')
	);
}    
	
function fnReady(){ 
	if(win.$('#'+outputID).length<=0){
		if(win.game_data.screen=='map'){ 
			var srcHTML = 
				'<div id="coord_picker">'
				+ '<input type="checkbox" id="cbBBEncode" onClick="isEncoded=this.checked; fnRefresh();"'
				+ (isEncoded?'checked':'')
				+ '/>BB k�ddal<br/>'
				+ '<textarea id="'
				+ outputID
				+ '" cols="40" rows="10" value="" onFocus="this.select();"/>'
				+ '</div>';
			
			ele=win.$('body').append(win.$(srcHTML));
			
			win.TWMap.map._handleClick=
				function(e){
					var pos=this.coordByEvent(e);
					var coord=pos.join("|");
					var ii=coords.indexOf(coord);
					if(ii>=0){
						coords.splice(ii,1);
					}else{
						coords.push(coord);
					}
					fnRefresh();
					return false;
				};
			
		}else{ 
			/*alert("Ez a script csak a t�rk�pen m�k�dik.\nUgr�s a t�rk�pre...");*/
			self.location=win.game_data.link_base_pure.replace(/screen\=\w*/i,"screen=map");
		}
	}
}	

win.$(win.document).ready(fnReady);

void(0);

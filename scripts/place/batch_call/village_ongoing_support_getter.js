//screen=info_village&id=<target village id>

//output: exportedSupportDetails (a complicated map)

//info:
//game.js: game/Command.js
//call Command.loadDetailsForTooltip(command, id) 
// on $('.own_command[data-command-type="support"]')
// where id is $(own_command).attr("data-command-id")
//Target object: Command.details_cache 
// after all Command.pending_details[command_id] is false

function isStillLoading() {
    for (var pendingCommand in Command.pending_details) {
        if (Command.pending_details[pendingCommand]) {
            return true;
        }
    }
    return false;
}
function exportOngoingSupportDetails(callback) {
    if (isStillLoading()) {
        setTimeout(function(){ exportOngoingSupportDetails(callback);}, 100);
    } else {
        exportedSupportDetails = Command.details_cache;
        console.log("exported ongoing support");
        if (typeof callback != "undefined" && callback != null) {
            callback(exportedSupportDetails);
        }
    }
}
function requestLoadDetailsForOngoingSupports(callback) {
    var supportItems = $('.own_command[data-command-type="support"]');
    for (var idx = 0; idx < supportItems.length; ++idx) {
        var supportItem = supportItems[idx];
        var commandId = parseInt($(supportItem).attr("data-command-id"));
        Command.loadDetailsForTooltip(supportItem, commandId);
    }
    exportOngoingSupportDetails(callback);
}
function startRequestLoadDetailsForOngoingSupports(callback) {
    if (window.$) {
        requestLoadDetailsForOngoingSupports(callback);
    } else {
        setTimeout(function() { 
            startRequestLoadDetailsForOngoingSupports(callback);
        }, 50);
    }
}
//requestLoadDetailsForOngoingSupports();
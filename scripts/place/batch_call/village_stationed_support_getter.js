//screen=info_village&id=<target village id>

//output: exportedUnitsStationed : map {unitType : int }

function getUnitMapIdx(headerRow) {
    var headerCells = $(headerRow).find("th,td");
    var unitMapIdx = {};
    for (var idx = 0; idx < headerCells.length; ++idx) {
        var unitType = $(headerCells[idx]).find("a").attr("data-unit");
        if (unitType) {
            unitMapIdx[unitType] = idx;
        }
    }
    return unitMapIdx;
}

//unfortunately no id:
function findStationedSupportTable() {
    var tables = $("#content_value").find("table");
    for (var idx = tables.length; idx >= 0; --idx) {
        var table = tables[idx];
        if ($(table).find(".unit-item").length > 0) {
            return table;
        }
    }
    return null;
}
function extractInt(cell) {
    return parseInt(cell.innerHTML);
}
function exportStationedSupport() {
    var table = findStationedSupportTable();
    if (table == null) {
        exportedUnitsStationed = {};
    } else {
        var rows = $(table).find("tr");
        var unitMapIdx = getUnitMapIdx(rows[0]);
        var unitData = {};
        for (var unitType in unitMapIdx) {
            unitData[unitType] = 0;
        }
        for (var rowIdx = 1; rowIdx < rows.length; ++rowIdx) {
            var row = rows[rowIdx];
            if ($(row).find("a").length > 0) { //skip home village units
                var cells = $(row).find("th,td");
                for (var unitType in unitMapIdx) {
                    var cellIdx = unitMapIdx[unitType];
                    unitData[unitType] += extractInt(cells[cellIdx]);
                }
            }
        }
        exportedUnitsStationed = unitData;
    }
    console.log("exported stationed support");
}
function startExportStationedSupport(callback) {
    if (window.$) {
        exportStationedSupport();
        callback(exportedUnitsStationed);
    } else {
        setTimeout(function(){ 
            startExportStationedSupport(callback); 
        }, 50);
    }
}
//exportStationedSupport();
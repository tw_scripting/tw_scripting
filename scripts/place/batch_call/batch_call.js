javascript:
var doc = document;
var myGlobal = this;

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, fromIndex) {
        "use strict";
        return jQuery.inArray(obj, this, fromIndex);
    };
}

//http://stackoverflow.com/questions/12199051/merge-two-arrays-of-keys-and-values-to-an-object-using-underscore
Array.prototype.toObj = function(values){
   values = values || this.map(function(v){return true;}); 
   var some;
   this .map(function(v){return [v,this.shift()]},values)
        .map(function(v){this[v[0]]=v[1];},some = {});
   return some; 
};

////////////////////////////////////////
//document factories
function createCheckbox(settings) {
    var checkbox = doc.createElement("input");
    checkbox.type = "checkbox";
    checkbox.name = settings.name;
    checkbox.checked = settings.isChecked;
    checkbox.onclick = settings.onclickFn;
    return checkbox;
}
function createButton(settings) {
    var button = doc.createElement("input");
    button.type = "button";
    button.value = settings.text;
    button.onclick = settings.onclickFn;
    button.className += "btn";
    button.id = settings.id;
    return button;
}
function createLink(settings) {
    var link = doc.createElement("a");
    link.innerHTML = settings.text;
    link.onclick = settings.onclickFn;
    return link;
}
function createInputText() {
	var edit = document.createElement("input");
	edit.type = "text";
	return edit;
}
function createTextArea(settings) {
    var textArea = doc.createElement("textarea");
    if (settings && typeof settings.rows != "undefined") {
        textArea.rows = settings.rows;
    }
    if (settings && typeof settings.cols != "undefined") {
        textArea.cols = settings.cols;
    }
    if (settings && typeof settings.readonly != "undefined") {
        textArea.readonly = settings.readonly;
    }
    return textArea;
}
function createRadioButton(settings) {
    var radio = doc.createElement("input");
    radio.type = "radio";
    radio.id = settings.id;
    radio.name = settings.name;
    radio.checked = settings.isChecked;
    radio.onclick = settings.onclickFn;
    return radio;
}
function createLabel(settings) {
    var label = doc.createElement("label");
    label.innerHTML = settings.text;
    if (settings.forElementName) {
        label.htmlFor = settings.forElementName;
    }
    return label;
}
function createImg(settings) {
    var img = doc.createElement("img");
    img.src = settings.src;
    return img;
}
function injectScriptIntoFrame(ifrm, injectedFn) {
	var frameDoc = ifrm.contentDocument;
	var injectedScript = frameDoc.createElement("script");
	injectedScript.type = "text/javascript";
	injectedScript.innerHTML = injectedFn.toString();
	var head = frameDoc.getElementsByTagName("head")[0];
	head.appendChild(injectedScript);
}
//OPTIONAL settings:
// id : string
// src : link string
// injectedScripts : array of functions or executable javascript string
// width : int
// height : int
// shouldHide : boolean
function createFrame(settings) {
    var frame = doc.createElement("IFRAME");
    function isSettingAvailable(setting) {
        return typeof setting != "undefined" && setting != null;
    }
    if (isSettingAvailable(settings.id)) {
        frame.id = settings.id;
    }
    if (isSettingAvailable(settings.injectedScripts)) {
        $(frame).load(
            function() {
                for (var idx = 0; idx < settings.injectedScripts.length; ++idx) {
                    injectScriptIntoFrame(frame, settings.injectedScripts[idx]);
                }
            }
        );
    }
    if (isSettingAvailable(settings.width) && 
        isSettingAvailable(settings.height)) 
    {
        frame.style.width = settings.width + "px";
        frame.style.height = settings.height + "px";
    }
    setFrameVisibility(frame, !settings.shouldHide);
    if (isSettingAvailable(settings.src)) {
        frame.src = settings.src;
    }
    return frame;
}
function setFrameVisibility(frame, isVisible) {
    if (isVisible) {
        frame.style.visibility = "visible";
        frame.style.display = "";
    } else {
        frame.style.visibility = "hidden";
        frame.style.display = "none";
    }
}

function insertUIElement(cell, element) {
    if (element.constructor === Array) {
        for (var idx = 0; idx < element.length; ++idx) {
            cell.appendChild(element[idx]);
        }
    } else {
        cell.appendChild(element);
    }
}
function addUIElement(row, element) {
    var cell = doc.createElement("td");
    row.appendChild(cell);
    insertUIElement(cell, element);
    return cell;
}
function appendTextToArea(textArea, text) {
    var resultText = $(textArea).val();
    if (resultText != "") {
        resultText += "\n";
    }
    resultText += text;
    $(textArea).val(resultText);
}

var mediaBaseUrl = "https://dl.dropboxusercontent.com/u/21199222/tw/script_media/";
var villageIcon = mediaBaseUrl + "village.png";
var inVillageIcon = mediaBaseUrl + "village_in.png";
var intoVillageIcon = mediaBaseUrl + "village_into.png";
var hereIcon = mediaBaseUrl + "here.png";

var dsBaseUrl = "https://dshu.innogamescdn.com/8.35.1/26862/graphic/";
var barrackIcon = dsBaseUrl + "buildings/barracks.png";
var supportIcon = dsBaseUrl + "command/support.png";
var playerIcon = dsBaseUrl + "welcome/player_points.png";

var loadingIcon = "loading2.gif";

var imgSrc = {
    spear :     "unit_spear.png",
    sword :     "unit_sword.png",
    axe :       "unit_axe.png",
    archer :    "unit_archer.png",
    spy :       "unit_spy.png",
    light :     "unit_light.png",
    marcher :   "unit_marcher.png",
    heavy :     "unit_heavy.png",
    ram :       "unit_ram.png",
    catapult :  "unit_catapult.png",
    knight :    "unit_knight.png",
    snob :      "unit_snob.png"
};

function getUnitImgSrc(unit) {
    return dsBaseUrl + 'unit/' + imgSrc[unit];
}

function getImg(icon) {
    return '<img src="' + icon + '"/>';
}
function getTWImage(icon) {
    return UI.Image(icon)[0];
}
function getOptionalParantheses(data, putParantheses) {
    var result = "" + data;
    if (putParantheses) {
        result = "(" + result + ")";
    }
    return result;
}
function getItalic(data) {
    return "<i>" + data + "</i>";
}
function getBold(data) {
    return "<b>" + data + "</b>";
}
function getUnderlined(data) {
    return "<u>" + data + "</u>";
}
function getStrikeThrough(data) {
    return "<s>" + data + "</s>";
}
function extractInt(cell) {
    return parseInt(cell.innerHTML);
}


//////////////////////////////////////////////
//parameters and localStorage:
var savedValueIndicator = "btch_cll_" + game_data.world;
function supportsHtml5Storage() {
    try {
        return "localStorage" in window && window["localStorage"] != null;
    } catch (e) {
        return false;
    }
}
var canUseStorage = supportsHtml5Storage();

function getParameterValue(parameterName, defaultValue) {
    var value = myGlobal[parameterName];
    if (typeof(value) == "undefined") {
        value = defaultValue;
    }
    return value;
}
function getSavedName(parameterName) {
    return savedValueIndicator + "_" + parameterName;
}
function saveValue(parameterName, value) {
    if (canUseStorage) {
        localStorage.setItem(getSavedName(parameterName), value);
        console.log("saving " + parameterName + " = " + value);
    }
}
function provideSavedValue(parameterName, defaultValue) {
    var value = null;
    if (canUseStorage) {
        value = localStorage.getItem(getSavedName(parameterName));
    }
    if (value == null) {
        value = getParameterValue(parameterName, defaultValue);
        saveValue(parameterName, value);
    }
    return value;
}

//global general data
var minutesPerField = {
    spear :    18, 
    sword :    22,
    axe :      18, 
    archer :   18, 
    spy :       9, 
    light :    10, 
    marcher :  10,
    heavy :    11,
    ram :      30, 
    catapult : 30,
    knight :   10, 
    snob :     35 
};

function getCoords(str) {
    return str.match(/\d{3}\|\d{3}/g);
}
function parseCoord(coordStr) {
    var coordSplit = coordStr.split("|");
    var coord = {
        x : parseInt(coordSplit[0]),
        y : parseInt(coordSplit[1])
    };
    return coord;
}
function getSquareDistance(coord1, coord2) {
    var dx = coord1.x - coord2.x;
    var dy = coord1.y - coord2.y;
    return dx*dx + dy*dy;
}
function getDistance(coord1, coord2) {
    return Math.sqrt(getSquareDistance(coord1, coord2));
}

/////////////////////////////////////
//unit call functionalities:

function isTableNecessary(table) {
    if (table.id != "") return true;
    if ($("a", table).length > 0) return true;
    return false;
}

function Village(villageRow, idx) {
    this.tableRow = villageRow;
    this.cells = $("td", villageRow);
    this.callButton = $(".call_button", villageRow)[0];
    
    this.isSelected = true;
    
    this.units = {};
    for (var idx = 0; idx < troopMgr.units.length; ++idx) {
        var unitType = troopMgr.units[idx];
        this.units[unitType] = parseInt(this.getUnitCell(unitType).innerHTML);
    }
    var $link = $(this.getCells()).find("a");
    var coords = getCoords($link.text());
    this.coord = coords[coords.length - 1];
    $.extend(this, parseCoord(this.coord));
    
    var villageIDParam = $link.attr("href").match(/&id=\d+/g)[0];
    this.villageID = villageIDParam.split("=")[1];
    
    this.setupUI(villageRow, idx);
}

Village.prototype.setupUI = function(villageRow, idx) {
    this.rowIdx = idx;
    var that = this;
    var selectedCheckBox = createCheckbox({
        name : "is_selected",
        isChecked : this.isSelected,
        onclickFn : function(){ troopMgr.toggleVillage(that.rowIdx); }
    });
    this.selectedCheckBox = selectedCheckBox;
    addUIElement(villageRow, selectedCheckBox);
    
    var burnDownButton = createLink({
        text : "\u25bc",
        onclickFn : function(){ troopMgr.burnDownSelection(that.rowIdx); }
    });
    addUIElement(villageRow, burnDownButton);
    
    var burnUpButton = createLink({
        text : "\u25b2",
        onclickFn : function(){ troopMgr.burnUpSelection(that.rowIdx); }        
    });
    addUIElement(villageRow, burnUpButton);
}

Village.prototype.toggleSelection = function() {
    this.setSelection(!this.isSelected);    
}

Village.prototype.setSelection = function(isSelected) {
    this.isSelected = isSelected;
    $(this.selectedCheckBox).prop("checked", this.isSelected);
    $(this.callButton).prop("disabled", !this.isSelected);
}

Village.prototype.getCells = function() {
    return this.cells;
}

Village.prototype.callUnitsHere = function(calledUnits) {
    if (!jQuery.isEmptyObject(calledUnits)) {
        this.callButton.click();
        for (var idx = 0; idx < troopMgr.units.length; ++idx) {
            var unitType = troopMgr.units[idx];
            var cell = this.getUnitCell(unitType);
            var textInput = $("input[type=text]", cell)[0];
            if (unitType in calledUnits) {
                textInput.value = calledUnits[unitType];
                this.units[unitType] -= calledUnits[unitType];
            } else {
                textInput.value = 0;
            }
        }
        this.callButton.click();
    }
}

Village.prototype.administerSentUnits = function(sentUnits) {
    for (var unitType in sentUnits) {
        this.units[unitType] -= sentUnits[unitType];
        var unitCell = this.getUnitCell(unitType);        
        unitCell.innerHTML = this.units[unitType];
    }
}

Village.prototype.recalculateDistance = function(targetCoord) {
    var distanceText = getDistance(this, targetCoord).toFixed(1);
    var distanceCell = this.getDistanceCell();
    $(distanceCell).text(distanceText);
}

Village.prototype.setRowIndex = function(idx) {
    this.rowIdx = idx;
}

function setupVillagePrototype(nameIdx, distanceIdx, unitMapIdx) {
    Village.prototype.getNameCell = function() {
        return this.getCells()[nameIdx];
    }
    Village.prototype.parseDistance = function() {
        return parseFloat(this.getCells()[distanceIdx].innerText);
    }
    Village.prototype.getDistanceCell = function() {
        return this.getCells()[distanceIdx];
    }
    Village.prototype.getUnitCell = function(unitType) {
        return this.getCells()[unitMapIdx[unitType]];
    }
}

function createCallback(fn, arg) {
    return function() { fn(arg); };
}

function areVillageDistancesInOrder(village1, village2, targetCoord) {
    var d1 = getSquareDistance(village1, targetCoord);
    var d2 = getSquareDistance(village2, targetCoord);
    return d1-d2;
}

function sortVillagesByDistance(villages, targetCoord) {
    function isOrderOk(village1, village2) { 
        return areVillageDistancesInOrder(village1, village2, targetCoord);
    }
    villages.sort(isOrderOk);
}

//REQUIRED settings:
// startPuffer: {unittype : int}
// maxUnits: {unittype : int}
// isUnitEnabled: {unittype : boolean}
// offerGetter: function(village, unitType) -> int
//OPTIONAL settings:
// acceptor : function(numPuffered, numMax, numOffered) -> int 
//  [default numOffered if omitted]
// villageEligibleGetter: function(village) -> boolean 
//  [default true if omitted]
// villageAction: function(village, collectedUnits { unittype : int})
//  [do nothing if omitted]
function UnitPuffer(settings) { 
    //required settings:
    this.puffered = settings.startPuffer;
    this.maxUnits = settings.maxUnits;
    this.isUnitEnabled = settings.isUnitEnabled;
    this.getOffer = settings.offerGetter;
    
    //optional settings:
    function isUndefined(setting) {
        return typeof setting == "undefined" || setting == null;
    }
    this.acceptOffer = settings.acceptor;
    if (isUndefined(this.acceptOffer)) {
        this.acceptOffer = function(numPuffered, numMax, numOffered) { return numOffered; };
    }
    this.isVillageEligible = settings.villageEligibleGetter;
    if (isUndefined(this.isVillageEligible)) {
        this.isVillageEligible = function(village) { return true; };
    }
    this.villageAction = settings.villageAction;
    if (isUndefined(this.villageAction)) {
        this.villageAction = function (village, collectedUnits) {};
    }
    
    //helper:
    this.enabledUnits = [];
    for (var unitType in this.puffered) {
        if (this.isUnitEnabled[unitType]) {
            this.enabledUnits.push(unitType);
        }
    }
    this.collected = $.extend({}, this.puffered);
    for (var unitType in this.collected) {
        this.collected[unitType] = 0;
    }
}
UnitPuffer.prototype.doWeNeedMore = function() {
    for (var idx = 0; idx < this.enabledUnits.length; ++idx) {
        var unitType = this.enabledUnits[idx];
        if (this.puffered[unitType] < this.maxUnits[unitType]) {
            return true;
        }
    }
    return false;
}
UnitPuffer.prototype.processVillage = function(village) {
    var collectedUnits = {};
    if (this.isVillageEligible(village)) {
        for (var idx = 0; idx < this.enabledUnits.length; ++idx) {
            var unitType = this.enabledUnits[idx];        
            var numPuffered = this.puffered[unitType];
            var numMax = this.maxUnits[unitType];
            var numOffered = this.getOffer(village, unitType);
            var numAccepted = this.acceptOffer(numPuffered, numMax, numOffered);
            if (numAccepted > 0) {
                collectedUnits[unitType] = numAccepted;
                this.puffered[unitType] += numAccepted;
                this.collected[unitType] += numAccepted;
            }
        }
        this.villageAction(village, collectedUnits);
    }
    return collectedUnits;
}
UnitPuffer.prototype.getCompatibleCollectedUnitsEnabled = function() {
    var compatibleUnitsEnabled = $.extend({}, this.isUnitEnabled);
    var effectiveSpeed = null;
    for (var unitType in this.collected) {
        if (this.collected[unitType] > 0) {
            if (effectiveSpeed == null) {
                effectiveSpeed = minutesPerField[unitType];
            } else {
                var candidateSpeed = minutesPerField[unitType];
                if (effectiveSpeed != candidateSpeed) {
                    compatibleUnitsEnabled[unitType] = false;
                } //else indifferent: if it was false, then the user did not want it
            }
        }
    }
    return compatibleUnitsEnabled;
}

//Helpers:
function isVillageSelected(village) {
    return village.isSelected;
}
function getFullOffer(village, unitType) {
    return village.units[unitType];
}
function acceptGreedy(numPuffered, numMax, numOffered) {
    return numOffered;
}
function acceptOnlyFit(numPuffered, numMax, numOffered) {
    if (numPuffered + numOffered  <= numMax) {
        return numOffered;
    } else {
        return 0;
    }
}
function acceptAdjusted(numPuffered, numMax, numOffered) {
    if (numPuffered + numOffered <= numMax) {
        return numOffered;
    } else {
        return numMax - numPuffered;
    }
}
function callUnitsFromVillage(village, calledUnits) {
    village.callUnitsHere(calledUnits);
}

//////////////////////////////////////////////////////////////
//iframe utilities
function getUnitMapIdx(headerRow) {
    var headerCells = $(headerRow).find("th,td");
    var unitMapIdx = {};
    for (var idx = 0; idx < headerCells.length; ++idx) {
        var unitType = $(headerCells[idx]).find("a").attr("data-unit");
        if (unitType) {
            unitMapIdx[unitType] = idx;
        }
    }
    return unitMapIdx;
}
//for units in our village:
function exportUnitDataHere() {
    var unitTable = $("#units_home")[0];
    var rows = $(unitTable).find("tr");
    var unitMapIdx = getUnitMapIdx(rows[0]);
    var homeRowCells = $(rows[1]).find("th,td");
    var totalSumCells = $(rows[rows.length - 1]).find("th,td");
    var unitData = {
        home : {},
        stationed : {}
    };
    for (var unitType in unitMapIdx) {
        var cellIdx = unitMapIdx[unitType];
        unitData.home[unitType] = extractInt(homeRowCells[cellIdx]);
        unitData.stationed[unitType] = 
            extractInt(totalSumCells[cellIdx]) -
            unitData.home[unitType];
    }
    exportedUnitDataHere = unitData;
    console.log("exported unit data here");
}
//for other village id by coord
function getVillageIdFromUrl() {
    return parseInt(self.location.href.match(/target=\d+/)[0].split("=")[1]);
}
function exportVillageId(oldId, callback) {
    var hastoRetry = false;
    try {
        exportedVillageId = getVillageIdFromUrl();
        if (exportedVillageId == oldId) {
            console.log("same old id...");
            hastoRetry = true;
        } else {
            //immediately clear village:
            $("#place_target").find(".village-item")[0].click();
            console.log("exported village id: " + exportedVillageId);
            if (typeof callback != "undefined" && callback != null) {
                callback(exportedVillageId);
            }
        }
    } catch (e) {
        hastoRetry = true;
        console.log("no id yet...");
    }
    if (hastoRetry) {
        setTimeout(function(){exportVillageId(oldId, callback);}, 100);
    }
}
function startExportVillageId(x, y, callback) {
    var oldId = -1;
    if ($("#place_target").find(".village-item").length > 0) {
        oldId = getVillageIdFromUrl();
    }
    $("#inputx")[0].value = x;
    $("#inputy")[0].value = y;
    exportVillageId(oldId, callback);
}
//for ongoing units into other village:
function isStillLoading() {
    for (var pendingCommand in Command.pending_details) {
        if (Command.pending_details[pendingCommand]) {
            return true;
        }
    }
    return false;
}
function exportOngoingSupportDetails(callback) {
    if (isStillLoading()) {
        setTimeout(function(){ exportOngoingSupportDetails(callback);}, 100);
    } else {
        exportedSupportDetails = Command.details_cache;
        console.log("exported ongoing support");
        if (typeof callback != "undefined" && callback != null) {
            callback(exportedSupportDetails);
        }
    }
}
function requestLoadDetailsForOngoingSupports(callback) {
    var supportItems = $('.own_command[data-command-type="support"]');
    for (var idx = 0; idx < supportItems.length; ++idx) {
        var supportItem = supportItems[idx];
        var commandId = parseInt($(supportItem).attr("data-command-id"));
        Command.loadDetailsForTooltip(supportItem, commandId);
    }
    exportOngoingSupportDetails(callback);
}
function startRequestLoadDetailsForOngoingSupports(callback) {
    if (window.$) {
        requestLoadDetailsForOngoingSupports(callback);
    } else {
        setTimeout(function() { 
            startRequestLoadDetailsForOngoingSupports(callback);
        }, 50);
    }
}
//for stationed units in other village:
function findStationedSupportTable() {
    var tables = $("#content_value").find("table");
    for (var idx = tables.length; idx >= 0; --idx) {
        var table = tables[idx];
        if ($(table).find(".unit-item").length > 0) {
            return table;
        }
    }
    return null;
}
function exportStationedSupport() {
    var table = findStationedSupportTable();
    if (table == null) {
        exportedUnitsStationed = {};
    } else {
        var rows = $(table).find("tr");
        var unitMapIdx = getUnitMapIdx(rows[0]);
        var unitData = {};
        for (var unitType in unitMapIdx) {
            unitData[unitType] = 0;
        }
        for (var rowIdx = 1; rowIdx < rows.length; ++rowIdx) {
            var row = rows[rowIdx];
            if ($(row).find("a").length > 0) { //skip home village units
                var cells = $(row).find("th,td");
                for (var unitType in unitMapIdx) {
                    var cellIdx = unitMapIdx[unitType];
                    unitData[unitType] += extractInt(cells[cellIdx]);
                }
            }
        }
        exportedUnitsStationed = unitData;
    }
    console.log("exported stationed support");
}
function startExportStationedSupport(callback) {
    if (window.$) {
        exportStationedSupport();
        callback(exportedUnitsStationed);
    } else {
        setTimeout(function(){ 
            startExportStationedSupport(callback); 
        }, 50);
    }
}

//for sending support:
function isBotCheckerActive() {
    return $("#bot_check_form").is(":visible");
}
function handleBotChecker(botCheckerDeactivatedCallback) {
    if (isBotCheckerActive()) {
        setTimeout(function(){ handleBotChecker(botCheckerDeactivatedCallback); }, 300);
    } else {
        botCheckerDeactivatedCallback();
    }
}
function sendSupportToOtherVillage(jsonUnits) {
    var units = JSON.parse(jsonUnits);
    for (var unitType in units) {
        $("#unit_input_" + unitType).val(units[unitType]);
    }
    $("#target_support").click();
}
function performSendSupportLogic(settings) {
    if ($("#command_target").find(".village-item").length > 0) {
        if (isBotCheckerActive()) {
            settings.botCheckerActivated();
            handleBotChecker(settings.botCheckerDeactivated);
        } else {
            settings.readyToSend();
        }
    } else if ($("#troop_confirm_go").length > 0){ 
        $("#troop_confirm_go").click();
    } else {
        settings.finishedSending();
    }
}
function startSendSupportLogic(settings) {
    if (window.$) {
        performSendSupportLogic(settings);
    } else {
        setTimeout(function(){ startSendSupportLogic(settings); }, 50);
    }
}

//////////////////////////////////////////////////////////////
//troop manager

var troopMgr = {
    allUnits : ["spear", "sword", "axe", "archer", "spy", "light", "marcher", "heavy", "ram", "catapult", "knight"],    
    unitParam : {},
    unitData : {
        here : {},
        other : {}
    },
    getMatchingUnit : function(str) {
        for (var idx = 0; idx < troopMgr.unitIndicators.length; ++idx) {
            indicator = troopMgr.unitIndicators[idx];
            if (str.indexOf(indicator) != -1) {
                return troopMgr.indicatorToUnit[indicator];
            }
        }
        return null;
    },    
    getTroopListTable : function() {
        return $("#village_troup_list");
    },
    setupParams : function() {
        troopMgr.ratio = parseFloat(provideSavedValue("ratio", 0.1));
        troopMgr.isRatioTroopModeSelected = (provideSavedValue("ratio_mode", 1) == "1");
        troopMgr.isTemplateTroopModeSelected = (provideSavedValue("group_mode", 0) == "1");
        troopMgr.hasToIncludeUnitsOngoing = (provideSavedValue("include_ongoing", 1) == "1");
        troopMgr.hasToIncludeUnitsStationed = (provideSavedValue("include_support", 1) == "1");
        troopMgr.hasToIncludeUnitsHome = (provideSavedValue("include_home", 0) == "1");
        troopMgr.hasToSeparateSpeed = (provideSavedValue("separate_speed", 0) == "1");
        troopMgr.isTargetHereModeSelected = true;
        troopMgr.isTargetOtherModeSelected = !troopMgr.isTargetHereModeSelected;
    },
    setupUnitParams : function () {
        troopMgr.unitParam.template = {};
        troopMgr.unitParam.max = {};
        for (var idx = 0; idx < troopMgr.units.length; ++idx) {
            var unitType = troopMgr.units[idx];
            troopMgr.unitParam.max[unitType] = parseInt(provideSavedValue("max_" + unitType, 10000000));
            troopMgr.unitParam.template[unitType] = parseInt(provideSavedValue("group_" + unitType, 100));
        }
    },
    setupUnits : function() {
        troopMgr.unitIndicators = troopMgr.allUnits.map(
            function(unitType){ return "order=" + unitType; }
        );
        troopMgr.indicatorToUnit = troopMgr.unitIndicators.toObj(troopMgr.allUnits);
        var units = [];
        var nameIdx;
        var distanceIdx;
        var unitMapIdx = {};
        troopMgr.unitParam.enabled = {};
        var headers = troopMgr.getTroopListTable().find("th,td");
        for (var idx = 0; idx < headers.length; ++idx) {
            var headerCell = headers[idx];
            var cellHTML = headerCell.innerHTML;
            matchingUnit = troopMgr.getMatchingUnit(cellHTML);
            if (matchingUnit) {
                unitMapIdx[matchingUnit] = idx;
                units.push(matchingUnit);
                var checkbox = $("input[type=checkbox]", headerCell)[0];
                troopMgr.unitParam.enabled[matchingUnit] = checkbox.checked;
                $(checkbox).click(createCallback(troopMgr.toggleUnitsEnabled, matchingUnit));
            } else if (cellHTML.indexOf("order=name") != -1) {
                nameIdx = idx;
            } else if (cellHTML.indexOf("order=distance") != -1) {
                distanceIdx = idx;
            }
        }
        //fallbacks if needed:
        if (typeof(nameIdx) == "undefined") {
            console.log("fallback to nameIdx = 0");
            nameIdx = 0;
        }
        if (typeof(distanceIdx) == "undefined") {
            console.log("fallback to distanceIdx = " + nameIdx + 1);
            distanceIdx = nameIdx + 1;
        }
        
        setupVillagePrototype(nameIdx, distanceIdx, unitMapIdx);
        troopMgr.unitMapIdx = unitMapIdx;
        troopMgr.units = units;
        troopMgr.setupUnitParams();
        troopMgr.unitData.here.home = troopMgr.getUnitMap(0);
        troopMgr.unitData.here.stationed = troopMgr.getUnitMap(0);
        
        troopMgr.unitData.other.home = troopMgr.getUnitMap(0);
        troopMgr.unitData.other.stationed = troopMgr.getUnitMap(0);
        troopMgr.unitData.other.ongoing = troopMgr.getUnitMap(0);
        
        troopMgr.unitData.actual = troopMgr.getTargetUnits();
    },
    getTargetUnits : function() {
        if (troopMgr.isTargetHereModeSelected) {
            return troopMgr.unitData.here;
        } else {
            return troopMgr.unitData.other;
        }
    },
    getTargetCoord : function() {
        if (troopMgr.isTargetHereModeSelected) {
            return game_data.village.coord;
        } else {
            return troopMgr.otherMgr.villageCoord;
        }
    },
    setupLogic : function() {
        var villageTable = troopMgr.getTroopListTable().find("tbody");
        var villageRows = $("tr", villageTable);
        troopMgr.villages = [];
        for (var idx = 0; idx < villageRows.length; ++idx) {
            var village = new Village(villageRows[idx], idx);
            troopMgr.villages.push(village);
        }
    },
    processUnitsHere : function(unitData) {
        troopMgr.unitData.here.home = $.extend({}, unitData.home);
        troopMgr.unitData.here.stationed = $.extend({}, unitData.stationed);
        $([troopMgr.ui.imgLoadingHome, troopMgr.ui.imgLoadingStationed]).hide();
        troopMgr.refresh();
    },
    ui : {
        dataElements : {},
        inputElements : {},
        getHiddenLoadingIcon : function() {
            var icon = getTWImage(loadingIcon);
            $(icon).hide();
            return icon;
        },
        createUnitElements : function(settings) {
            var elementMap = {};
            for (var idx = 0; idx < troopMgr.units.length; ++idx) {
                var unitType = troopMgr.units[idx];
                var element = settings.creator(unitType);
                elementMap[unitType] = element;
                var unitCell = addUIElement(settings.row, element);
            }
            return elementMap;
        },
        createUnitInputs : function(settings) {
            return troopMgr.ui.createUnitElements({
                row : settings.row,
                creator : function(unitType) {
                    var unitEdit = createInputText();
                    unitEdit.value = settings.valueMap[unitType];
                    unitEdit.onkeyup = createCallback(settings.onkeyupFn, unitType);
                    unitEdit.size = 5;
                    return unitEdit;
                }
            });
        },
        createUnitLabels : function(row) {
            return troopMgr.ui.createUnitElements({
                row : row,
                creator : function(unitType) {
                    return createLabel({
                        text : ""
                    });
                }
            });
        },
        addLabelRow : function(settings) {
            var row = settings.table.insertRow();
            var leadCell = row.insertCell();
            leadCell.width = troopMgr.ui.rowLeadMaxWidth;
            leadCell.innerHTML = settings.leadContent;
            troopMgr.ui.dataElements[settings.dataAspect] = 
                troopMgr.ui.createUnitLabels(row);
        },
        
        removeUnnecessaryElements : function() {
            //make some place
            var contentValue = $("#content_value");
            var tables = $(contentValue).find("table");
            //make sure it is still that fancy-design title-table:
            if (!isTableNecessary(tables[0])) {
                $(tables[0]).remove();
            }
            $(contentValue).find("h3").remove();
        },
        setupVillageExtension : function() {
            var headerRow = troopMgr.getTroopListTable().find("thead").find("tr")[0];
            troopMgr.ui.checkboxSelectAllVillages = createCheckbox({
                name : "selected_all",
                isChecked : true,
                onclickFn : troopMgr.toggleAllSelection
            });
            addUIElement(headerRow, troopMgr.ui.checkboxSelectAllVillages);
        },
        setupTargetModeSwitch : function(targetModeCell) {
            troopMgr.ui.radioButtonTargetHere = createRadioButton({
                id : "target_mode_here",
                name : "target_mode",
                isChecked : troopMgr.isTargetHereModeSelected,
                onclickFn : troopMgr.targetHereModeSelected
            });
            var labelTargetHere = createLabel({
                text : getImg(hereIcon),
                forElementName : "target_mode_here"
            });
            troopMgr.ui.radioButtonTargetOther = createRadioButton({
                id : "target_mode_other",
                name : "target_mode",
                isChecked : troopMgr.isTargetOtherModeSelected,
                onclickFn : troopMgr.targetOtherModeSelected
            });
            var labelTargetOther = createLabel({
                text : getImg(playerIcon),
                forElementName : "target_mode_other"
            });
            insertUIElement(targetModeCell, [
                troopMgr.ui.radioButtonTargetHere,
                labelTargetHere,
                createLabel({text : " /"}),
                troopMgr.ui.radioButtonTargetOther,
                labelTargetOther
                ]
            );
        },
        setupOngoingIncluder : function(cell) {
            troopMgr.ui.checkboxIncludeOngoing = createCheckbox({
                name : "include_units_incoming",
                isChecked : troopMgr.hasToIncludeUnitsOngoing,
                onclickFn : troopMgr.toggleIncludeOngoing
            });
            troopMgr.ui.imgLoadingOngoing = troopMgr.ui.getHiddenLoadingIcon();            
            insertUIElement(cell, [
                troopMgr.ui.checkboxIncludeOngoing, 
                createImg({src : supportIcon}), 
                createImg({src : intoVillageIcon}),
                troopMgr.ui.imgLoadingOngoing
                ]
            );
        },
        setupOngoingRow : function(ongoingRow) {
            var ongoingCell = ongoingRow.insertCell(0);
            ongoingCell.width = troopMgr.ui.rowLeadMaxWidth;
            troopMgr.ui.setupOngoingIncluder(ongoingCell);
            //store ongoing cells
            var cells = $(ongoingRow).find("td");
            troopMgr.ui.dataElements.ongoing = {};
            for (var colIdx = 0; colIdx < cells.length; ++colIdx) {
                var cell = cells[colIdx];
                var unitType = $(cell).attr("data-unit");
                if (unitType) {
                    troopMgr.ui.dataElements.ongoing[unitType] = cell;
                }
            }
        },
        addHomeRow : function(table) {
            var homeRow = table.insertRow();
            troopMgr.ui.checkboxIncludeHome = createCheckbox({
                name : "include_units_home",
                isChecked : troopMgr.hasToIncludeUnitsHome,
                onclickFn : troopMgr.toggleIncludeHome
            });
            troopMgr.ui.imgLoadingHome = troopMgr.ui.getHiddenLoadingIcon();
            var homeCell = addUIElement(homeRow, [
                troopMgr.ui.checkboxIncludeHome, 
                createImg({src : barrackIcon}), 
                createImg({src : inVillageIcon}),
                troopMgr.ui.imgLoadingHome
                ]
            );
            homeCell.width = troopMgr.ui.rowLeadMaxWidth;
            troopMgr.ui.dataElements.home = troopMgr.ui.createUnitLabels(homeRow);
        },
        addStationedRow : function(table) {
            var stationedRow = table.insertRow();
            troopMgr.ui.checkboxIncludeStationed = createCheckbox({
                name : "include_units_stationed",
                isChecked : troopMgr.hasToIncludeUnitsStationed,
                onclickFn : troopMgr.toggleIncludeStationed
            });
            troopMgr.ui.imgLoadingStationed = troopMgr.ui.getHiddenLoadingIcon();
            var stationedCell = addUIElement(stationedRow, [
                troopMgr.ui.checkboxIncludeStationed, 
                createImg({src : supportIcon}), 
                createImg({src : inVillageIcon}),
                troopMgr.ui.imgLoadingStationed
                ]
            );
            stationedCell.width = troopMgr.ui.rowLeadMaxWidth;
            troopMgr.ui.dataElements.stationed = 
                troopMgr.ui.createUnitLabels(stationedRow);
        },
        addCollectedRow : function(table) {
            troopMgr.ui.addLabelRow({
                table : table,
                leadContent : "+ " + getImg(supportIcon),
                dataAspect : "collected"
            });
        },
        addSumRow : function(table) {
            troopMgr.ui.addLabelRow({
                table : table,
                leadContent : "=" + "\u2211",
                dataAspect : "sum"
            });
        },
        addCallableRow : function(table) {
            troopMgr.ui.addLabelRow({
                table : table,
                leadContent : "MAX + " + getImg(supportIcon),
                dataAspect : "callable"
            });
        },
        addMaxRow : function(table) {
            var maxRow = table.insertRow();
            var maxCell = maxRow.insertCell();
            maxCell.width = troopMgr.ui.rowLeadMaxWidth;
            maxCell.innerHTML = "MAX " + "\u2211";
            troopMgr.ui.inputElements.max = troopMgr.ui.createUnitInputs({
                row : maxRow,
                valueMap : troopMgr.unitParam.max,
                onkeyupFn : troopMgr.maxUnitsChanged
            });
        },
        addTemplateRow : function(table, modeName) {
            var templateRow = table.insertRow();
            var modeID = "template" + "_" + modeName;
            troopMgr.ui.radioButtonTemplateMode = createRadioButton({
                id : modeID,
                name : modeName,
                isChecked : troopMgr.isTemplateTroopModeSelected,
                onclickFn : troopMgr.templateTroopModeSelected
            });
            var labelTemplateTroop = createLabel({
                text : getImg(supportIcon) + " / " + getImg(villageIcon),
                forElementName : modeID
            });
            troopMgr.ui.buttonTemplateTroop = createButton({
                text : "\u2713",
                onclickFn : troopMgr.troopTemplateCallAction,
                id : "template_troop_action"
            });
            var templateCell = addUIElement(templateRow, [
                troopMgr.ui.radioButtonTemplateMode, 
                labelTemplateTroop, 
                troopMgr.ui.buttonTemplateTroop
                ]
            );
            templateCell.width = troopMgr.ui.rowLeadMaxWidth;
            troopMgr.ui.inputElements.template = troopMgr.ui.createUnitInputs({
                row : templateRow,
                valueMap : troopMgr.unitParam.template,
                onkeyupFn : troopMgr.templateUnitsChanged
            });
        },
        addRatioRow : function(table, modeName) {
            var ratioRow = table.insertRow();
            var modeID = "ratio" + "_" + modeName;
            troopMgr.ui.radioButtonRatioMode = createRadioButton({
                id : modeID,
                name : modeName,
                isChecked : troopMgr.isRatioTroopModeSelected,
                onclickFn : troopMgr.ratioTroopModeSelected
            });
            var labelRatio = createLabel({
                text : "% / " + getImg(villageIcon),
                forElementName : modeID
            });
            var inputRatio = createInputText();
            troopMgr.ui.inputRatio = inputRatio;
            inputRatio.value = troopMgr.ratio * 100;
            inputRatio.size = 1;
            inputRatio.onkeyup = createCallback(troopMgr.ratioChanged);
            troopMgr.ui.buttonRatioTroop = createButton({
                text : "\u2713",
                onclickFn : troopMgr.ratioCallAction,
                id : "ratio_troop_action"
            });
            var ratioCell = addUIElement(ratioRow, [
                troopMgr.ui.radioButtonRatioMode, 
                inputRatio, 
                labelRatio, 
                troopMgr.ui.buttonRatioTroop
                ]
            );
            ratioCell.width = troopMgr.ui.rowLeadMaxWidth;
        },
        addSeparateSpeedRow : function(table) {
            var separateSpeedRow = table.insertRow();
            troopMgr.ui.checkboxMixSpeed = createCheckbox({
                name : "separate_speed",
                isChecked : !troopMgr.hasToSeparateSpeed,
                onclickFn : troopMgr.toggleSeparateSpeed
            });
            var separateSpeedCell = addUIElement(separateSpeedRow, [
                createLabel({text : "("}),
                createImg({src : getUnitImgSrc("spear")}), 
                createLabel({text : "&"}),
                createImg({src : getUnitImgSrc("heavy")}),
                createLabel({text : "&"}),
                createImg({src : getUnitImgSrc("catapult")}),
                createLabel({text : "&...)"}),
                troopMgr.ui.checkboxMixSpeed
                ]
            );
            separateSpeedCell.width = troopMgr.ui.rowLeadMaxWidth;
        },
        setupSupportTable : function() {
            var supportTable = $("#support_sum")[0];
            var supportRows = $("tr", supportTable);
            var headerWidth = $(supportRows[0]).find("td,th")[0].width;
            //this must be set first:
            troopMgr.ui.rowLeadMaxWidth = Math.round(headerWidth * 2.8); 
            
            troopMgr.ui.removeUnnecessaryElements();
            troopMgr.ui.setupVillageExtension();
            
            var targetModeCell = supportRows[0].insertCell(0);
            targetModeCell.width = troopMgr.ui.rowLeadMaxWidth;
            troopMgr.ui.setupTargetModeSwitch(targetModeCell);
            
            troopMgr.ui.setupOngoingRow(supportRows[1]);
            
            troopMgr.ui.addHomeRow(supportTable);
            troopMgr.ui.addStationedRow(supportTable);
            troopMgr.ui.addCollectedRow(supportTable);
            troopMgr.ui.addSumRow(supportTable);
            troopMgr.ui.addMaxRow(supportTable);
            troopMgr.ui.addCallableRow(supportTable);
            troopMgr.ui.addRatioRow(supportTable, "troop_mode");
            troopMgr.ui.addTemplateRow(supportTable, "troop_mode");
            troopMgr.ui.addSeparateSpeedRow(supportTable);
            
            troopMgr.getTroopListTable().find("td,th").css('white-space','nowrap');
        },
        setupOtherTable : function() {
            var table = doc.createElement("table");
            $(table).hide();
            var otherRow = table.insertRow();
            troopMgr.ui.textAreaOtherCoords = createTextArea({
                rows : 5,
                cols : 40
            });
            troopMgr.ui.buttonAddCoords = createButton({
                text : "\u25b6\u25b6",
                onclickFn : troopMgr.otherMgr.addTargetCoords,
                id : "add_target_coords"
            });            
            troopMgr.ui.selectorOtherCoordPool = doc.createElement("select");
            troopMgr.ui.selectorOtherCoordPool.size = 5;
            troopMgr.ui.selectorOtherCoordPool.ondblclick = 
                troopMgr.otherMgr.coordSelectorDblClicked;
            troopMgr.ui.selectorOtherCoordPool.onchange = 
                troopMgr.otherMgr.coordSelectorOnChange;
            
            troopMgr.ui.buttonGoOther = createButton({
                text : "GO",
                onclickFn : troopMgr.otherMgr.go,
                id : "go_to_other_target"
            });
            troopMgr.ui.buttonStopOther = createButton({
                text : "\u2713 \u25b6\u25b6",
                onclickFn : troopMgr.otherMgr.targetDone,
                id : "other_target_done"
            });
            troopMgr.ui.textAreaOtherSummary = createTextArea({
                rows : 5,
                cols : 40
            });
            addUIElement(otherRow, [
                createImg({src : playerIcon}),
                createImg({src : villageIcon}),
                createLabel({text : ":"}),
                troopMgr.ui.textAreaOtherCoords,
                troopMgr.ui.buttonAddCoords,
                troopMgr.ui.selectorOtherCoordPool,
                troopMgr.ui.buttonGoOther,
                troopMgr.ui.buttonStopOther
                ]
            );       
            addUIElement(otherRow, [
                createLabel({text : "\u2211"}),
                createImg({src : supportIcon}),
                createLabel({text : ":"}),
                troopMgr.ui.textAreaOtherSummary
                ]
            );
            return table;
        },
        setupUI : function() {
            troopMgr.ui.setupSupportTable();
            
            troopMgr.ui.otherTable = troopMgr.ui.setupOtherTable();
            $(troopMgr.ui.otherTable).insertAfter("#support_sum");
            
            var infoMessage = "<a target='_blank' href='https://forum.klanhaboru.hu/showthread.php?3657-Csapatok-bizonyos-sz%C3%A1zal%C3%A9k%C3%A1nak-%C3%B6sszeh%C3%ADv%C3%A1sa'>[Csapatösszehívó]</a>";
            $("#content_value").prepend(infoMessage + "<br>");
            
            troopMgr.uiReady = true;
            //TODO:
            //time constraint (start, end)
        },
        dataPresenter : {
            home : function(data) {
                var res = getOptionalParantheses(data, !troopMgr.hasToIncludeUnitsHome);
                if (troopMgr.isTargetOtherModeSelected) {
                    res = getStrikeThrough(res);
                }
                return res;
            },
            stationed : function(data) { 
                return getOptionalParantheses(data, !troopMgr.hasToIncludeUnitsStationed);
            },
            collected : getBold,
            sum : getUnderlined,
            callable : getItalic
        },
        //REQUIRED:
        // unitData : { unitType : int }
        // dataAspect : string
        presentUnitData : function(settings) {
            var transformator = troopMgr.ui.dataPresenter[settings.dataAspect];
            if (typeof transformator == "undefined" || transformator == null) {
                transformator = function(data) { return data; };
            }
            var dataElements = troopMgr.ui.dataElements[settings.dataAspect];
            for (var unitType in dataElements) {
                var data;
                if (unitType in settings.unitData) {
                    data = settings.unitData[unitType];
                } else {
                    data = 0;
                }
                dataElements[unitType].innerHTML = transformator(data);
            }
        },
        showOtherInput : function() {
            $(troopMgr.ui.otherTable).show();
            setFrameVisibility(troopMgr.otherMgr.senderFrame, true);
        },
        hideOtherInput : function() {
            $(troopMgr.ui.otherTable).hide();
            setFrameVisibility(troopMgr.otherMgr.senderFrame, false);
        },
        setEnabledOtherInput : function(isEnabled) {
            $([
                troopMgr.ui.buttonAddCoords, 
                troopMgr.ui.buttonGoOther, 
                troopMgr.ui.buttonStopOther]
            ).prop("disabled", !isEnabled);
            troopMgr.ui.selectorOtherCoordPool.disabled = !isEnabled;
        },
        blockOtherInput : function() {
            troopMgr.ui.setEnabledOtherInput(false);
        },
        enableOtherInput : function() {
            troopMgr.ui.setEnabledOtherInput(true);
            $(troopMgr.ui.buttonGoOther).focus();
        },
        addCoordToPool : function(coord) {
            var option = new Option(coord, coord);
            $(troopMgr.ui.selectorOtherCoordPool).append(option);
        },
        removeCoordFromSelector : function(coord) {
            $(troopMgr.ui.selectorOtherCoordPool).find('option[value="' + coord + '"]').remove();
        },
        getSelectedCoord : function() {
            if (troopMgr.ui.selectorOtherCoordPool.options.length > 0) {
                var selectedIdx = troopMgr.ui.selectorOtherCoordPool.selectedIndex;
                var selectedCoord = troopMgr.ui.selectorOtherCoordPool.options[selectedIdx].value;
                return selectedCoord; 
            } else {
                return null;
            }
        }
    },
    otherMgr : {
        isInputSetup : false,
        targetCoordPool : [],
        resetLogic : function() {
            troopMgr.otherMgr.villageCoord = null;
            troopMgr.otherMgr.villageID = null;
            troopMgr.otherMgr.restartVillages();
            troopMgr.unitData.other.stationed = troopMgr.getUnitMap(0);
            troopMgr.unitData.other.ongoing = troopMgr.getUnitMap(0);
            troopMgr.refresh();
            $([troopMgr.ui.imgLoadingOngoing]).hide();
            console.log("other logic was reset");
        },
        setupIdGetterFrame : function() {
            troopMgr.otherMgr.villageIDGetterFrame = createFrame({
                id : "get_other_id",
                src : game_data.link_base_pure + "place",
                shouldHide : true,
                width : 400,
                height : 400,
                injectedScripts : [
                    exportVillageId,
                    getVillageIdFromUrl,
                    startExportVillageId
                ],
            });
            $("#content_value").prepend(troopMgr.otherMgr.villageIDGetterFrame);
        },
        setupVillageFrame : function() {
            troopMgr.otherMgr.villageFrame = createFrame({
                id : "other_village",
                shouldHide : true,
                width : 600,
                height : 400,
                injectedScripts : [
                    //for stationed units:
                    getUnitMapIdx,
                    extractInt,
                    findStationedSupportTable,
                    exportStationedSupport,
                    startExportStationedSupport,
                    'startExportStationedSupport(window.parent.troopMgr.otherMgr.processStationedUnits)',
                    
                    //for ongoing units:
                    isStillLoading,
                    exportOngoingSupportDetails,
                    requestLoadDetailsForOngoingSupports,
                    startRequestLoadDetailsForOngoingSupports,
                    'startRequestLoadDetailsForOngoingSupports(window.parent.troopMgr.otherMgr.processOngoingUnits)'
                ]
            });
            $("#content_value").prepend(troopMgr.otherMgr.villageFrame);
        },
        setupSenderFrame : function() {
            troopMgr.otherMgr.senderFrame = createFrame({
                id : "other_sender",
                shouldHide : false, //we need this because of FireFox
                width : 700,
                height : 200,
                injectedScripts : [
                    isBotCheckerActive,
                    handleBotChecker,
                    sendSupportToOtherVillage,
                    performSendSupportLogic,
                    startSendSupportLogic,
                    function doIt() {
                        startSendSupportLogic({
                            readyToSend : 
                                window.parent.troopMgr.otherMgr.sendUnitsWithFrame, 
                            finishedSending :
                                window.parent.troopMgr.otherMgr.finishedSending,
                            botCheckerActivated : 
                                window.parent.troopMgr.otherMgr.botCheckerActivated,
                            botCheckerDeactivated : 
                                window.parent.troopMgr.otherMgr.botCheckerDeactivated                            
                        });
                    },
                    'doIt();'
                ]
            });
            $(troopMgr.otherMgr.senderFrame).insertAfter($(troopMgr.ui.otherTable));
        },
        setup : function() {
            troopMgr.otherMgr.isInputSetup = true;
            troopMgr.otherMgr.setupIdGetterFrame();
            troopMgr.otherMgr.setupVillageFrame();
            troopMgr.otherMgr.setupSenderFrame();
            troopMgr.otherMgr.resetLogic();
        },

        restartVillages : function() {
            troopMgr.otherMgr.actVillageIdx = null;
        },
        nextVillage : function() {
            var village = null;
            var idx = troopMgr.otherMgr.actVillageIdx;
            if (idx == null) {
                idx = 0;
            } else {
                ++idx;
            }
            if (idx >= troopMgr.villages.length) {
                idx = null;
            } else {
                village = troopMgr.villages[idx];
            }
            troopMgr.otherMgr.actVillageIdx = idx;
            //TODO mark village
            return village;
        },
        
        //////////////////////////////////////
        //collect information of other village
        retrieveVillageInformation : function() {
            var selectedCoord = troopMgr.ui.getSelectedCoord();
            if (selectedCoord != null) {
                troopMgr.ui.blockOtherInput();
                $([troopMgr.ui.imgLoadingOngoing, troopMgr.ui.imgLoadingStationed]).show();
                troopMgr.otherMgr.villageCoord = selectedCoord;
                var coord = parseCoord(selectedCoord);
                troopMgr.otherMgr.retrieveVillageID(coord.x, coord.y);
            }
        },
        retrieveVillageID : function(x, y) {
            injectScriptIntoFrame(
                troopMgr.otherMgr.villageIDGetterFrame, 
                "startExportVillageId(" + x + "," + y + ", window.parent.troopMgr.otherMgr.processVillageID)"
            );
        },
        processVillageID : function(villageID) {
            troopMgr.otherMgr.villageID = villageID;
            console.log("troopMgr.otherMgr.otherVillageID : " + troopMgr.otherMgr.villageID);
            troopMgr.otherMgr.retrieveVillageUnitData(villageID);
            troopMgr.refreshDistances();
        },
        retrieveVillageUnitData : function(targetVillageID) {
            troopMgr.otherMgr.unitDataRetrieved = {
                stationed : false,
                ongoing : false
            };
            troopMgr.otherMgr.villageFrame.src = 
                game_data.link_base_pure + "info_village&id=" + targetVillageID;
        },
        isDataReady : function() {
            return troopMgr.otherMgr.unitDataRetrieved.stationed &&
                troopMgr.otherMgr.unitDataRetrieved.ongoing;
        },
        processStationedUnits : function(stationedUnits) {
            troopMgr.unitData.other.stationed = troopMgr.getUnitMap(0);
            $.extend(troopMgr.unitData.other.stationed, stationedUnits);
            troopMgr.otherMgr.unitDataRetrieved.stationed = true;
            console.log("troopMgr.unitData.other.stationed processed");
            $(troopMgr.ui.imgLoadingStationed).hide();
            troopMgr.otherMgr.refreshIfDataReady();
        },
        processOngoingUnits : function(exportedSupportDetails) {
            var ongoingUnits = troopMgr.getUnitMap(0);
            for (var commandID in exportedSupportDetails) {
                var command = exportedSupportDetails[commandID];
                if (command.type == "support") {
                    for (var unitType in command.units) {
                        var numUnits = parseInt(command.units[unitType].count);
                        ongoingUnits[unitType] += numUnits;
                    }
                }
            }
            troopMgr.unitData.other.ongoing = ongoingUnits;
            troopMgr.otherMgr.unitDataRetrieved.ongoing = true;
            console.log("troopMgr.unitData.other.ongoing processed");
            $(troopMgr.ui.imgLoadingOngoing).hide();
            troopMgr.otherMgr.refreshIfDataReady();
        },
        refreshIfDataReady : function() {
            if (troopMgr.otherMgr.isDataReady()) {
                troopMgr.refresh();
                troopMgr.ui.enableOtherInput();
            }
        },
                
        //////////////////////////////
        //send units to other village
        sendUnits : function(sourceVillage, targetVillageID, sentUnits) {
            var sourceVillageID = sourceVillage.villageID;
            troopMgr.ui.blockOtherInput();
            sourceVillage.administerSentUnits(sentUnits);
            for (var unitType in sentUnits) {
                troopMgr.unitData.other.ongoing[unitType] += sentUnits[unitType];
            }
            troopMgr.otherMgr.sentUnits = sentUnits; //TODO resolve this implicit dependency
            $([troopMgr.ui.imgLoadingOngoing]).show();
            troopMgr.otherMgr.senderFrame.src = game_data.link_base_pure + "place" + 
                "&village=" + sourceVillageID + "&target=" + targetVillageID;
            
        },
        botCheckerActivated : function() {
            UI.ErrorMessage("Bot védelem: írd be a kis ablakon a kódot!", 4000);
        },
        botCheckerDeactivated : function() {
            UI.SuccessMessage("Folytatás...", 1500);
            troopMgr.otherMgr.sendUnitsWithFrame();
        },
        sendUnitsWithFrame : function() {
            injectScriptIntoFrame(
                troopMgr.otherMgr.senderFrame, 
                "sendSupportToOtherVillage('" + JSON.stringify(troopMgr.otherMgr.sentUnits) + "');"
            );
        },
        finishedSending : function() {
            troopMgr.refresh(); //todo include villages only after actual village
            $([troopMgr.ui.imgLoadingOngoing]).hide();
            troopMgr.ui.enableOtherInput();
        },
        
        //Tries to process villages one by one until it finds units to send.
        //Returns true if can send anything, false otherwise.
        sendUnitsOnce : function(senderPuffer) { 
            var sentUnits = {};
            var actVillage = null;
            do {
                actVillage = troopMgr.otherMgr.nextVillage();
                if (actVillage != null) {
                    sentUnits = senderPuffer.processVillage(actVillage);
                }
            } while (actVillage != null && $.isEmptyObject(sentUnits));
            if (!$.isEmptyObject(sentUnits)) { //if there is anything to send
                troopMgr.otherMgr.sendUnits(actVillage, troopMgr.otherMgr.villageID, sentUnits);
                return true;
            } else { //in this case actVillage is null -> no more villages to process
                return false;
            }
        },
        removeTargetCoord : function(coord) {
            //remove from ui selector:
            troopMgr.ui.removeCoordFromSelector(coord);
            //remove from logic:
            var removedIdx = troopMgr.otherMgr.targetCoordPool.indexOf(coord);
            if (removedIdx > -1) {
                troopMgr.otherMgr.targetCoordPool.splice(removedIdx, 1);
            }
        },
        appendSummary : function() {
            var summary = "";
            summary += troopMgr.otherMgr.villageCoord + "\n";

            var relevantUnits = [];
            var otherUnitData = troopMgr.unitData.other;
            for (var unitType in otherUnitData.ongoing) {
                if (otherUnitData.ongoing[unitType] > 0 || otherUnitData.stationed[unitType] > 0) {
                    relevantUnits.push(unitType);
                }
            }
            var summaryData = {
                stationed : [],
                ongoing : [],
                sum : []
            };
            for (var idx = 0; idx < relevantUnits.length; ++idx) {
                var unitType = relevantUnits[idx];
                summaryData.stationed.push(otherUnitData.stationed[unitType]);
                summaryData.ongoing.push(otherUnitData.ongoing[unitType]);
                summaryData.sum.push( 
                    otherUnitData.stationed[unitType] + otherUnitData.ongoing[unitType]
                );
            }
            
            var headerUnits = relevantUnits.map(function(unitType){ 
                return "[unit]" + unitType + "[/unit]";
            });
            var sumUnits = summaryData.sum.map(function(num){
                return "[b]" + num + "[/b]";
            });
            summary += "[table]\n";
            summary += "[**][||]" + headerUnits.join("[||]") + "[/**]\n";
            summary += "[*].[|]" + summaryData.stationed.join("[|]") + "\n";
            summary += "[*]+[|]" + summaryData.ongoing.join("[|]") + "\n";
            summary += "[*][b]=\u2211[/b][|]" + sumUnits.join("[|]") + "\n";
            summary += "[/table]\n";
            appendTextToArea(troopMgr.ui.textAreaOtherSummary, summary);
        },        
        
        ////////////////
        //event handlers
        addTargetCoords : function() {
            var textArea = $(troopMgr.ui.textAreaOtherCoords);
            var input = textArea.val();
            textArea.val("");
            var coords = getCoords(input);
            if (coords != null) {
                for (var idx = 0; idx < coords.length; ++idx) {
                    var coord = coords[idx];
                    if (troopMgr.otherMgr.targetCoordPool.indexOf(coord) == -1) {
                        //add to logic
                        troopMgr.otherMgr.targetCoordPool.push(coord);
                        //add to ui selector
                        troopMgr.ui.addCoordToPool(coord);
                    }
                }
            }
        },
        coordSelectorOnChange : function() {
            troopMgr.otherMgr.retrieveVillageInformation();
        },
        coordSelectorDblClicked : function() {
            var selectedCoord = troopMgr.ui.getSelectedCoord();
            //stop send-logic
            if (troopMgr.otherMgr.villageCoord == selectedCoord) {
                troopMgr.otherMgr.resetLogic();
            }
            //add back to ui input:
            appendTextToArea(troopMgr.ui.textAreaOtherCoords, selectedCoord);
            
            troopMgr.otherMgr.removeTargetCoord(selectedCoord);
        },
        go : function() {
            if (troopMgr.otherMgr.villageCoord == null) {
                if (troopMgr.otherMgr.targetCoordPool.length > 0) {
                    if (troopMgr.ui.selectorOtherCoordPool.selectedIndex == -1) {
                        troopMgr.ui.selectorOtherCoordPool.selectedIndex = 0;
                    }
                    troopMgr.otherMgr.retrieveVillageInformation();
                    troopMgr.otherMgr.restartVillages();
                }
            } else {
                var senderPuffer = new UnitPuffer(troopMgr.getSupportSettings());
                if (senderPuffer.doWeNeedMore()) {
                    var haveSent = troopMgr.otherMgr.sendUnitsOnce(senderPuffer);
                    if (haveSent) {
                        if (troopMgr.hasToSeparateSpeed && !senderPuffer.doWeNeedMore()) {
                            UI.InfoMessage("Egysebességű egységek kiküldve, újra...", 3000);
                            troopMgr.otherMgr.restartVillages();
                        }
                    } else {
                        UI.InfoMessage("Még nincs elég a maxhoz, újra...", 3000);
                        troopMgr.otherMgr.restartVillages();
                    }
                } else { //no need to send more
                    troopMgr.otherMgr.targetDone();
                }
            }
        },
        targetDone : function() {
            if (troopMgr.otherMgr.villageCoord != null) {
                UI.SuccessMessage("Következő...", 1500);
                troopMgr.otherMgr.restartVillages();
                troopMgr.otherMgr.appendSummary();
                troopMgr.otherMgr.removeTargetCoord(troopMgr.otherMgr.villageCoord);
                troopMgr.otherMgr.villageCoord = null;
                troopMgr.otherMgr.go(); //call ourselves again to select next target (if any)
            }            
        }
    },
    retrieveUnitsHere : function() {
        $([troopMgr.ui.imgLoadingHome, troopMgr.ui.imgLoadingStationed]).show();
        var frame = createFrame({
            id : "unit_sum",
            src : game_data.link_base_pure + "place&mode=units",
            shouldHide : true,
            width : 600,
            height : 400,
            injectedScripts : [
                getUnitMapIdx,
                extractInt,
                exportUnitDataHere,
                function doIt() {
                    exportUnitDataHere();
                    window.parent.troopMgr.processUnitsHere(exportedUnitDataHere);
                },
                'doIt();'
            ]
        });
        $("#content_value").prepend(frame);
    },
    setup : function() {
        if (game_data.screen != "place" || game_data.mode != "call") {
            UI.InfoMessage("Csapatok kérése...", 4000);
            self.location=game_data.link_base_pure + "place&mode=call";
            return;
        }
        troopMgr.setupParams();
        troopMgr.setupUnits();
        troopMgr.ui.setupUI();
        troopMgr.retrieveUnitsHere(); //async
        troopMgr.setupLogic();
        troopMgr.refreshTroopModePermissions();
        troopMgr.refresh();
        setInterval(troopMgr.periodicRefresh, 1000);
    },
    getUnitMap : function(defaultValue) {
        var result = {};
        for (var idx = 0; idx < troopMgr.units.length; ++idx) {
            result[troopMgr.units[idx]] = defaultValue;
        }
        return result;
    },
    getOngoingUnits : function() {
        if (troopMgr.isTargetHereModeSelected) {
            var ongoing = {};
            for (var unitType in troopMgr.ui.dataElements.ongoing) {
                ongoing[unitType] = extractInt(troopMgr.ui.dataElements.ongoing[unitType]);
            }
            return ongoing;
        } else {
            return troopMgr.unitData.other.ongoing;
        }
    },
    getIncludedUnits : function() {
        var included = troopMgr.getUnitMap(0);
        var ongoingUnits = troopMgr.getOngoingUnits();
        for (var unitType in included) {
            if (troopMgr.hasToIncludeUnitsOngoing) {
                included[unitType] += ongoingUnits[unitType];
            }
            if (troopMgr.hasToIncludeUnitsHome) {
                included[unitType] += troopMgr.unitData.actual.home[unitType];
            }
            if (troopMgr.hasToIncludeUnitsStationed) {
                included[unitType] += troopMgr.unitData.actual.stationed[unitType];
            }
        }
        return included;
    },
    //helpers used for UnitPuffer:
    getOfferByRatio : function(village, unitType) {
        return Math.round(village.units[unitType] * troopMgr.ratio);
    },
    getOfferByTemplate : function(village, unitType) {
        var desiredNum = troopMgr.unitParam.template[unitType];
        if (village.units[unitType] < desiredNum) {
            return 0;
        } else {
            return desiredNum;
        }
    },
    getTroopModeSettings : function() {
        if (troopMgr.isRatioTroopModeSelected) {
            return {
                offerGetter : troopMgr.getOfferByRatio,
                acceptor : acceptAdjusted
            };
        } else {
            return {
                offerGetter : troopMgr.getOfferByTemplate,
                acceptor : acceptOnlyFit
            };
        }
    },
    getEffectiveSettings : function() {
        return {
            startPuffer : troopMgr.getIncludedUnits(),
            maxUnits : $.extend({}, troopMgr.unitParam.max),
            isUnitEnabled : $.extend({}, troopMgr.unitParam.enabled),
            villageEligibleGetter : isVillageSelected
        };
    },
    runPuffer : function(settings) {
        var puffer = new UnitPuffer(settings);
        for (var idx = 0; idx < troopMgr.villages.length && puffer.doWeNeedMore(); ++idx) {
            puffer.processVillage(troopMgr.villages[idx]);
        }
        return puffer;
    },
    runCallablePuffer : function() {
        var settings = {
            startPuffer : troopMgr.getUnitMap(0),
            maxUnits : troopMgr.getUnitMap(Infinity),
            isUnitEnabled : troopMgr.getUnitMap(true),
            offerGetter : getFullOffer
        };
        return troopMgr.runPuffer(settings);
    },
    runPredictionPuffer : function() {
        var settings = jQuery.extend({},
            troopMgr.getTroopModeSettings(),
            troopMgr.getEffectiveSettings()
        );
        return troopMgr.runPuffer(settings);
    },
    refresh : function() {
        if (!troopMgr.uiReady) {
            console.log("ui not ready, skipping");
            return;
        }
        if (troopMgr.isRefreshing) {
            console.log("ongoing refresh, skipping");
            return;
        } else {
            troopMgr.isRefreshing = true;
        }
        var predictionPuffer = troopMgr.runPredictionPuffer();
        var callablePuffer = troopMgr.runCallablePuffer();
        var dataSource = {
            home : troopMgr.unitData.actual.home,
            stationed : troopMgr.unitData.actual.stationed,
            collected : predictionPuffer.collected,
            sum : predictionPuffer.puffered,
            callable : callablePuffer.puffered
        };
        if (troopMgr.isTargetOtherModeSelected) {
            dataSource.ongoing = troopMgr.unitData.other.ongoing;
        }
        for (var dataAspect in dataSource) {
            troopMgr.ui.presentUnitData({
                unitData : dataSource[dataAspect],
                dataAspect : dataAspect
            });
        }
        troopMgr.isRefreshing = false;
    },
    periodicRefresh : function() {
        if (troopMgr.isTargetHereModeSelected) {
            troopMgr.refresh();
        }
    },
    applySpeedSeparationSettings : function(settings) {
        if (troopMgr.hasToSeparateSpeed) {
            var predictionPuffer = troopMgr.runPredictionPuffer();
            var effectiveUnitsEnabled = predictionPuffer.getCompatibleCollectedUnitsEnabled();
            settings.isUnitEnabled = effectiveUnitsEnabled;
        }
    },
    getSupportSettings : function() {
        var settings = jQuery.extend({},
            troopMgr.getTroopModeSettings(),
            troopMgr.getEffectiveSettings()
        );
        troopMgr.applySpeedSeparationSettings(settings);
        return settings;
    },
    callUnits : function() {
        var settings = troopMgr.getSupportSettings();        
        settings.villageAction = callUnitsFromVillage;
        var puffer = troopMgr.runPuffer(settings);
        troopMgr.refresh();
    },
    refreshTroopModePermissions : function() {
        if (troopMgr.isTargetHereModeSelected) {
            $(troopMgr.ui.buttonRatioTroop).prop("disabled", !troopMgr.isRatioTroopModeSelected);
            $(troopMgr.ui.buttonTemplateTroop).prop("disabled", troopMgr.isRatioTroopModeSelected);
        } else {
            $([troopMgr.ui.buttonRatioTroop, troopMgr.ui.buttonTemplateTroop]).prop("disabled", true);
        }
    },
    refreshDistances : function() {
        var targetCoord = troopMgr.getTargetCoord();
        if (targetCoord) {
            var parsedCoord = parseCoord(targetCoord);
            for (var idx = 0; idx < troopMgr.villages.length; ++idx) {
                troopMgr.villages[idx].recalculateDistance(parsedCoord);
            }
            troopMgr.resortVillages(parsedCoord);
        }
    },
    resortVillages : function(parsedCoord) {
        sortVillagesByDistance(troopMgr.villages, parsedCoord);
        var villageTable = troopMgr.getTroopListTable();
        for (var idx = 0; idx < troopMgr.villages.length; ++idx) {
            var village = troopMgr.villages[idx];
            village.setRowIndex(idx);
            villageTable.children("tbody").append(village.tableRow);
        }
    },
    targetModeChanged : function () {
        if (troopMgr.isTargetOtherModeSelected) {            
            if (!troopMgr.otherMgr.isInputSetup) {
                troopMgr.otherMgr.setup();
            }
            troopMgr.ui.showOtherInput();
        } else {
            troopMgr.ui.hideOtherInput();
        }
        troopMgr.refreshTroopModePermissions();
        troopMgr.refreshDistances();
        troopMgr.unitData.actual = troopMgr.getTargetUnits();
        troopMgr.ui.presentUnitData({
            unitData : troopMgr.unitData.actual.ongoing,
            dataAspect : "ongoing"
        });
        troopMgr.refresh();
    },
    targetHereModeSelected : function() {
        troopMgr.isTargetHereModeSelected = true;
        troopMgr.isTargetOtherModeSelected = false;
        troopMgr.targetModeChanged();
    },
    targetOtherModeSelected : function() {
        //caching:
        troopMgr.unitData.here.ongoing = troopMgr.getOngoingUnits();
        
        troopMgr.isTargetHereModeSelected = false;
        troopMgr.isTargetOtherModeSelected = true;
        troopMgr.targetModeChanged();
    },
    toggleIncludeOngoing : function() {
        troopMgr.hasToIncludeUnitsOngoing = troopMgr.ui.checkboxIncludeOngoing.checked;
        saveValue("include_ongoing", troopMgr.hasToIncludeUnitsOngoing ? 1 : 0);
        troopMgr.refresh();
    },
    toggleIncludeStationed : function() {
        troopMgr.hasToIncludeUnitsStationed = troopMgr.ui.checkboxIncludeStationed.checked;
        saveValue("include_support", troopMgr.hasToIncludeUnitsStationed ? 1 : 0);
        troopMgr.refresh();
    },
    toggleIncludeHome : function() {
        troopMgr.hasToIncludeUnitsHome = troopMgr.ui.checkboxIncludeHome.checked;
        saveValue("include_home", troopMgr.hasToIncludeUnitsHome ? 1 : 0);
        troopMgr.refresh();
    },
    toggleSeparateSpeed : function() {
        troopMgr.hasToSeparateSpeed = !troopMgr.ui.checkboxMixSpeed.checked;
        saveValue("separate_speed", troopMgr.hasToSeparateSpeed ? 1 : 0);
    },
    ratioCallAction : function() {
        troopMgr.ratioTroopModeSelected();
        troopMgr.callUnits();
    },
    troopTemplateCallAction : function() {
        troopMgr.templateTroopModeSelected();
        troopMgr.callUnits();
    },
    ratioTroopModeSelected : function() {
        troopMgr.ui.radioButtonRatioMode.checked = true;
        troopMgr.isRatioTroopModeSelected = true;
        troopMgr.isTemplateTroopModeSelected = false;
        saveValue("ratio_mode", 1);
        saveValue("group_mode", 0);
        troopMgr.refreshTroopModePermissions();
        troopMgr.refresh();
    },
    templateTroopModeSelected : function() {
        troopMgr.ui.radioButtonTemplateMode.checked = true;
        troopMgr.isRatioTroopModeSelected = false;
        troopMgr.isTemplateTroopModeSelected = true;
        saveValue("ratio_mode", 0);
        saveValue("group_mode", 1);
        troopMgr.refreshTroopModePermissions();
        troopMgr.refresh();
    },
    ratioChanged : function() {
        troopMgr.refresh();
        var input = troopMgr.ui.inputRatio.value;
        input = input.replace(",",".");
        var ratio = parseFloat(input)/100.0;
        troopMgr.ratio = ratio;
        if (!isNaN(ratio)) {
            if (ratio > 1) {
                ratio = 1.0;
                troopMgr.ui.inputRatio.value = 100;
            }
            troopMgr.ratio = ratio;
            troopMgr.refresh();
            saveValue("ratio", ratio);
        }                
    },
    templateUnitsChanged : function(unitType) {
        var newSize = parseInt(troopMgr.ui.inputElements.template[unitType].value);
        if (!isNaN(newSize)) {
            troopMgr.unitParam.template[unitType] = newSize;
            troopMgr.refresh();
            saveValue("group_" + unitType, newSize);
        }
    },
    maxUnitsChanged : function(unitType) {
        var newMax = parseInt(troopMgr.ui.inputElements.max[unitType].value);
        if (!isNaN(newMax)) {
            troopMgr.unitParam.max[unitType] = newMax;
            troopMgr.refresh();
            saveValue("max_" + unitType, newMax);
        }
    },
    toggleVillage : function(idx) {
        troopMgr.villages[idx].toggleSelection();
        troopMgr.refresh();
    },
    selectVillages : function(settings) {
        for (var idx = settings.from; idx <= settings.to; ++idx) {
            troopMgr.villages[idx].setSelection(settings.isSelected);
        }
        troopMgr.refresh();
    },
    toggleAllSelection : function() {
        troopMgr.selectVillages({
            from : 0, 
            to : troopMgr.villages.length - 1,
            isSelected : troopMgr.ui.checkboxSelectAllVillages.checked
        });
    },
    burnDownSelection : function(firstSelected) {
        troopMgr.selectVillages({
            from : 0, 
            to : firstSelected - 1,
            isSelected : false
        });
        troopMgr.ui.checkboxSelectAllVillages.checked = false;
    },
    burnUpSelection : function(lastSelected) {
        troopMgr.selectVillages({
            from : lastSelected + 1, 
            to : troopMgr.villages.length - 1,
            isSelected : false
        });
        troopMgr.ui.checkboxSelectAllVillages.checked = false;
    },
    toggleUnitsEnabled : function(unitType) {
        troopMgr.unitParam.enabled[unitType] = !troopMgr.unitParam.enabled[unitType];
        troopMgr.refresh();
    }
};

function advancedBatchCall() {
    troopMgr.setup();
}

advancedBatchCall();
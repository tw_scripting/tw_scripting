//screen=place&village=<fromID>&target=<toID>

function isBotCheckerActive() {
    return $("#bot_check_form").is(":visible");
}
function handleBotChecker(botCheckerDeactivatedCallback) {
    if (isBotCheckerActive()) {
        setTimeout(function(){ handleBotChecker(botCheckerDeactivatedCallback); }, 300);
    } else {
        botCheckerDeactivatedCallback();
    }
}
function sendSupportToOtherVillage(jsonUnits) {
    var units = JSON.parse(jsonUnits);
    for (var unitType in units) {
        $("#unit_input_" + unitType).val(units[unitType]);
    }
    $("#target_support").click();
}
function performSendSupportLogic(settings) {
    if ($("#command_target").find(".village-item").length > 0) {
        if (isBotCheckerActive()) {
            settings.botCheckerActivated();
            handleBotChecker(settings.botCheckerDeactivated);
        } else {
            settings.readyToSend();
        }
    } else if ($("#troop_confirm_go").length > 0){ 
        $("#troop_confirm_go").click();
    } else {
        settings.finishedSending();
    }
}
function startSendSupportLogic(settings) {
    if (window.$) {
        performSendSupportLogic(settings);
    } else {
        setTimeout(function(){ startSendSupportLogic(settings); }, 50);
    }
}
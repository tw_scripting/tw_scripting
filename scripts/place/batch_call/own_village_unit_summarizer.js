//screen=place&mode=units

//output: unitData { 
//          home { unitType : int }, 
//          stationed {unitType : int }
//        }

function getUnitMapIdx(headerRow) {
    var headerCells = $(headerRow).find("th,td");
    var unitMapIdx = {};
    for (var idx = 0; idx < headerCells.length; ++idx) {
        var unitType = $(headerCells[idx]).find("a").attr("data-unit");
        if (unitType) {
            unitMapIdx[unitType] = idx;
        }
    }
    return unitMapIdx;
}
function extractInt(cell) {
    return parseInt(cell.innerHTML);
}
function exportUnitDataHere() {
    var unitTable = $("#units_home")[0];
    var rows = $(unitTable).find("tr");
    var unitMapIdx = getUnitMapIdx(rows[0]);
    var homeRowCells = $(rows[1]).find("th,td");
    var totalSumCells = $(rows[rows.length - 1]).find("th,td");
    var unitData = {
        home : {},
        stationed : {}
    };
    for (var unitType in unitMapIdx) {
        var cellIdx = unitMapIdx[unitType];
        unitData.home[unitType] = extractInt(homeRowCells[cellIdx]);
        unitData.stationed[unitType] = 
            extractInt(totalSumCells[cellIdx]) -
            unitData.home[unitType];
    }
    exportedUnitDataHere = unitData;
    console.log("exported unit data here");
}
exportUnitDataHere();
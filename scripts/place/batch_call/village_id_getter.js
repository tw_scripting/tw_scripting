//screen=place

//output: exportedVillageId (int)

function getVillageIdFromUrl() {
    return parseInt(self.location.href.match(/target=\d+/)[0].split("=")[1]);
}
function exportVillageId(oldId, callback) {
    var hastoRetry = false;
    try {
        exportedVillageId = getVillageIdFromUrl();
        if (exportedVillageId == oldId) {
            console.log("same old id...");
            hastoRetry = true;
        } else {
            //immediately clear village:
            $("#place_target").find(".village-item")[0].click();
            console.log("exported village id: " + exportedVillageId);
            if (typeof callback != "undefined" && callback != null) {
                callback(exportedVillageId);
            }
        }
    } catch (e) {
        hastoRetry = true;
        console.log("no id yet...");
    }
    if (hastoRetry) {
        setTimeout(function(){exportVillageId(oldId, callback);}, 100);
    }
}
function startExportVillageId(x, y, callback) {
    var oldId = -1;
    if ($("#place_target").find(".village-item").length > 0) {
        oldId = getVillageIdFromUrl();
    }
    $("#inputx")[0].value = x;
    $("#inputy")[0].value = y;
    exportVillageId(oldId, callback);
}
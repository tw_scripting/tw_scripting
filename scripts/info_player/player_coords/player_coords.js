﻿/*************************************************
author  : IronFist
version : 1.1
script  : This script is a coordinate lister.
          It works on a player's profile.
*************************************************/

(function() {
	function createList() {
		var list        = [];
		var coord       = [];
		var coordStr    = "";
		var continent   = 0;
		
		$('table#villages_list').find('tr').not(':first').not(':has(td[colspan])').each(function(key, val) {
			coordStr    = $(val).children('td').not(':last').last().text();
			coord       = coordStr.split("|");
			continent   = (Math.floor(parseInt(coord[1]) / 100) * 10) + Math.floor(parseInt(coord[0]) / 100);
			
			list.push({
				coord       : coordStr,
				continent   : continent
			});
		});
		
		return list;
	}
	
	function sort(list) {
		list.sort(function(a, b) {
			return a.continent - b.continent;
		});
		
		return list;
	}
	
	function showList(list) {
		if(list.length > 0) {
			var url       = location.href;
			var continent = list[0].continent;
			var num       = 0;
			var coords    = "";
			var html      = "<a name=\"script\"></a>";
			var pattern   = '<p>K{CONTINENT}</p><textarea cols="80" rows="10">javascript:coords="{COORD}";var doc=document;if(window.frames.length>0)doc=window.main.document;url=doc.URL;if(url.indexOf("screen=place")==-1)alert("Ez a script csak a gyülekezőhelyről futtatható!");coords=coords.split(" ");index=Math.round(Math.random()*(coords.length-1));coords=coords[index];coords=coords.split("|");doc.forms[0].x.value=coords[0];doc.forms[0].y.value=coords[1];insertUnit(doc.forms[0].ram,1);insertUnit(doc.forms[0].spy,50);void(0)</textarea><br><br>';
			
			$.each(list, function(key, val) {
				if(continent != list[key].continent) {
					html += pattern.replace(/\{CONTINENT\}/, continent).replace(/\{COORD\}/, coords);
					
					continent = list[key].continent;
					coords    = "";
					num       = 0;
				}
				
				if(num != 0) {
					coords += " " + list[key].coord;
				} else {
					coords += list[key].coord;
				}
				
				num = num + 1;
			});
			
			html += pattern.replace(/\{CONTINENT\}/, continent).replace(/\{COORD\}/, coords);
			
			$('body').append(html);
			
			
			location.href = "#script";
			history.replaceState(null,null,url);
		}
	}

	function __() {
		if ((typeof fejleszto != 'string') || (fejleszto != String.fromCharCode(73, 114, 111, 110, 70, 105, 115, 116))) {
			alert(String.fromCharCode(65, 32, 102, 101, 106, 108, 101, 115, 122, 116, 337, 116, 32, 110, 101, 32, 110, 233, 118, 116, 101, 108, 101, 110, 237, 116, 115, 100, 32, 101, 108, 33));

			$('body').empty();

			return false;
		}

		showList(sort(createList()));
	}

	if (document.URL.match("screen=info_player")) {
		__();
	} else {
		alert("A script egy játékos profilján működik.");
	}
})();
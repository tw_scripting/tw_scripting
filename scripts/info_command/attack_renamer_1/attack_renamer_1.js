﻿
var theTable;
var theRowIdxToInsert;
var theDoc;
var theInputEdit;

var Const3600 = 3600;
var Const60 = 60;
var ConstWidth = 'width';
	
function fnGetConfig(){
	var oRequest = new XMLHttpRequest();
	var sURL = "http://"+window.location.hostname + "/interface.php?func=get_config";
	oRequest.open("GET",sURL,0); oRequest.send(null);
	if(oRequest.status==200){
		return oRequest.responseXML;
	} else {
		alert("Error executing XMLHttpRequest call to get Config!");
	}
}

function myGetCoords(str){
	return/(.*?)\s\(((\d+)\|(\d+))\)/i.exec(str);
}

function myZeroPad(strVal){
	var intVal = parseInt(strVal,10);
	return (intVal>9 ? ''+intVal : '0'+intVal);
}

function myGetElementsByTagName(obj,tagName){
	return obj.getElementsByTagName(tagName);
}

function myGetInner(obj){
	return obj.innerHTML;
}

//This function will return the first <a> tag's caption->first link
function myGetInnerofFirstLink(obj){
	return myGetInner(myGetElementsByTagName(obj,'a')[0]);
}

function myInsRow(){
	return theTable.insertRow(theRowIdxToInsert++);
}

function myInsCell(rowObj, cellIdx){
	return rowObj.insertCell(cellIdx);
}

function mySetInner(obj, strVal){
	obj.innerHTML = strVal;
	return obj;
}

function myInsertTheHeader(rowObj, headerStr){
	var newCell = rowObj.appendChild(theDoc.createElement('th'));
	return mySetInner(newCell,headerStr);
}

function mySecondsToTimeStr(intVal){
	return ''+myZeroPad(intVal/Const3600)+':'+myZeroPad(intVal%(Const3600)/Const60 )+':'+myZeroPad(intVal%Const60);
}

function myTimeStrToSeconds(strVal) {
	var arrTime = strVal.match(/\d+/ig);
	var result = (arrTime[0]*Const3600 + arrTime[1]*Const60 + arrTime[2]*1);
	return result;
}

function myDateToString(dtDate){
	var intMs=dtDate.getMilliseconds();
	var result = '' + myZeroPad(dtDate.getHours())+':'+
		myZeroPad(dtDate.getMinutes())+':'+
		myZeroPad(dtDate.getSeconds())+'.'+
		(intMs>99?intMs:'0'+myZeroPad(intMs))+' '+
		myZeroPad(dtDate.getDate())+'/'+myZeroPad(dtDate.getMonth()+1);
	return result;
}

function strToInt(strVal){
	return parseInt(strVal,10);
}

function myTWDateStrToDate(strDate){
	//remove MilliSec, if it is there.
	var arrMs = strDate.match(/:(\d{3})$/i);
	if(arrMs){
		strDate = strDate.replace(/:(\d{3})$/i,'');
	}
	var dtNew = new Date(strDate);

	if(dtNew=='Invalid Date'){
		var arrDate = strDate.match(/\b(\d+)\b/ig); //create an array of consecutive numberstrings
		arrDate = arrDate.map(strToInt); //convert these strings to numbers

		if(arrDate[2]<2000){ //year
			arrDate[2] += 2000;
		}
		dtNew = new Date(arrDate[2],arrDate[1]-1,arrDate[0],arrDate[3],arrDate[4],arrDate[5]);
	}
	if(arrMs){
		dtNew.setMilliseconds(arrMs[1]);
	}
	return dtNew;
}

//main function to execute:
function labelAttack(){
	//if the user did not set theFormat variable
	if(typeof(theFormat) == 'undefined'){
		theFormat = '{unit}, {origin}';
	}

	var arrUnitSpeeds = [9,10,11,18,22,30,35];
	var arrUnitNames = ['Feld','KLov','NLov','Bárdos','KardForgató','Fkos','FN'];
	var arrFormatFields = ['unit','coords','player','distance','sent','duration','arrival','origin', 'destination','destinationxy'];
	var arrHead = ['Unit','Sent (ago)','Duration','Name To'];
	var arrValues = arrFormatFields;	
	
	arrFormatFields = arrFormatFields.map( //wrap formatFields into "{}"
		function (txtString){
			return new RegExp("\{"+txtString+"\}","ig");
		}
	);
	
	var xmlDoc = fnGetConfig();
	var theUnitSpeed = xmlDoc.getElementsByTagName('unit_speed')[0].childNodes[0].nodeValue;
	var theWorldSpeed = xmlDoc.getElementsByTagName('speed')[0].childNodes[0].nodeValue; 
	
	var doc=document;
	if(window.frames.length>1){
		doc=window.main.document;
	}
	theDoc = doc;
	
	var url=doc.URL;		
	theTable = theDoc.getElementById('edit').parentNode.parentNode.parentNode.parentNode;
	theTable.removeAttribute(ConstWidth);
	var arrRows = theTable.rows;
	var numRows = arrRows.length;

	//each row gets aligned to 5: the last cells' colSpan takes the difference 
	for(rowIdx in arrRows){
		var actRow = arrRows[rowIdx];
		var arrCells = actRow.cells;
		var numCells;
		if (arrCells) {
			numCells = arrCells.length;
		} else {
			numCells = 0;
		}
		if(numCells > 0){
			arrCells[numCells-1].colSpan = 5-numCells;
		}
	}
		
	arrValues[2] = myGetInnerofFirstLink(arrRows[1].cells[2]); //set player name
	var arrAttack = myGetCoords(myGetInnerofFirstLink(arrRows[2].cells[1]));
	var arrTarget = myGetCoords(myGetInnerofFirstLink(arrRows[4].cells[1]));
	arrValues[1] = arrAttack[2]; //set attacker village coords
	arrValues[7] = arrAttack[0]; //set attacker village name (origin)
	arrValues[8] = arrTarget[0]; //set destination village name
	arrValues[9] = arrTarget[2]; //set destination coords
	
	//set arrival date:
	var dateHolderObject = arrRows[numRows-3].cells[1];
	arrValues[6] = 
		typeof(dateHolderObject.innerText)=='undefined' ?
			  dateHolderObject.textContent
			: dateHolderObject.innerText;
	var dtArrival = myTWDateStrToDate(arrValues[6]); //create date object
	arrValues[6] = myDateToString(dtArrival);
	
	var strArrivalIn = myGetInner(arrRows[6].cells[1]);
	var msecsArrivalIn = myTimeStrToSeconds(strArrivalIn) * 1000;
	
	var xDiff = arrAttack[3]-arrTarget[3];
	var yDiff = arrAttack[4]-arrTarget[4];
	var dblDistance = Math.sqrt(xDiff*xDiff + yDiff*yDiff);
	arrValues[3] = dblDistance.toFixed(2); //set distance
	
	theRowIdxToInsert = numRows-2; //insert before remaining time's  row
	var arrElements = myGetElementsByTagName(theTable,'input');
	var inputButton = arrElements[1];
	theInputEdit = arrElements[0];
	theInputEdit.size = Const60;
	
	//row for distance:
	var newRow = myInsRow();
	mySetInner(myInsCell(newRow, 0), 'Távolság:').colSpan = 2;
	mySetInner(myInsCell(newRow, 1), arrValues[3] + ' Mező').colSpan = 2;
	
	//row for unit-table's header
	newRow = myInsRow();
	for (cellIdx in arrHead) {
		myInsertTheHeader(newRow, arrHead[cellIdx]);
	}
	
	//for each unit:
	for(unitIdx in arrUnitNames){
		//this unit takes this much time for this distance:
		var msecsDuration = Math.round(arrUnitSpeeds[unitIdx]*Const60*1000*dblDistance/theWorldSpeed/theUnitSpeed);
		
		//in this very moment we know how much is left before arrival
		//we only add a row for each unit which takes more to arrive (its duration is longer) than the time left before arrival.
		if (msecsDuration > msecsArrivalIn) { 
			var secsDiff = (msecsDuration-msecsArrivalIn)/1000; 
			arrValues[0] = arrUnitNames[unitIdx]; 			//set the unit name
			arrValues[5] = mySecondsToTimeStr(msecsDuration/1000); //set duration
			var dtSent = new Date(dtArrival-msecsDuration);
			arrValues[4] = myDateToString(dtSent);			//set date (when the command was sent)
			
			newRow = myInsRow(); //create row for this unit
			mySetInner(myInsCell(newRow, 0), arrUnitNames[unitIdx]); //cell for unit name
			
			//cell for time (sent how much time ago):
			mySetInner(myInsCell(newRow, 1), 
				secsDiff<Const60&&'just now'
				||
				secsDiff<Const3600&&Math.floor(secsDiff/Const60)+' mins ago'
				||
				mySecondsToTimeStr(secsDiff)+' ago');
			
			//cell for full duration of this unit:
			mySetInner(myInsCell(newRow, 2), arrValues[5]);
			
			//cell for OK button and textinput for rename command:
			var newCell = myInsCell(newRow, 3);
			var newButton = newCell.appendChild(inputButton.cloneNode(true));
			var newInput = newCell.appendChild(theInputEdit.cloneNode(true));
			newInput.id = 'I' + unitIdx;
			newInput.value = theFormat; //take every needed field (format)
			
			//replace every format-field to the associated value stored in arrValues
			for (formatFieldIdx in arrFormatFields) {
				newInput.value = newInput.value.replace(arrFormatFields[formatFieldIdx], arrValues[formatFieldIdx]);
			}
			newButton.onmousedown = new Function('theInputEdit.value=theDoc.getElementById(\'I' + unitIdx + '\').value;');
		}
	}
}
labelAttack();

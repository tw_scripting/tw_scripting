/*************************************************
author  : IronFist
version : 4.1
script  : This script can be used to accept offers
          on the market.
*************************************************/

(function () {
	var mainSearch = beallitasok.keres.replace("fa", "wood").replace("agyag", "stone").replace("vas", "iron").replace("összes", "all");
	var mainOffer = beallitasok.kinal.replace("fa", "wood").replace("agyag", "stone").replace("vas", "iron").replace("összes", "all");

	//Nyersanyag beállítása
	function setResType() {
		$('form#offer_filter').children('table').first().find('table').first().find('input').filter('[value="' + mainSearch + '"]').prop("checked", true);
		$('form#offer_filter').children('table').first().find('table').last().find('input').filter('[value="' + mainOffer + '"]').prop("checked", true);

		$('form#offer_filter').trigger("submit");
	}

	//Kijelölt nyersanyag keresése
	function getResType() {
		var ret = {};

		ret.search = $('form#offer_filter').children('table').first().find('table').first().find('input').filter('[value="' + mainSearch + '"]').prop("checked");
		ret.offer = $('form#offer_filter').children('table').first().find('table').last().find('input').filter('[value="' + mainOffer + '"]').prop("checked");

		return ret;
	}

	//Kijelölt nyersanyag ellenőrzése
	function checkResType() {
		var resType = getResType();

		if (!resType.search || !resType.offer) {
			return false;
		} else {
			return true;
		}
	}

	function acceptOffer(obj) {
		var traders = $('form#offer_filter').parent().children('table').first().find('th').first().text().replace("Kereskedők: ", "").split("/");
		var premiumTrading = $('form#offer_filter').parent().children('table').first().find('th').length == 3;
		var moveableInTheory = parseInt(traders[0]) * 1000;
		var allInTheory = parseInt(traders[1]) * 1000;
		var offer = 0;
		var price = 0;
		var available = 0;
		var number = 0;
		var coming = allInTheory - moveableInTheory;
		var required = (Math.round(obj.mennyiseg / 1000) * 1000) - coming;
		var moveable = Math.min(moveableInTheory, parseInt($('span#' + mainOffer).text()));

		if (required <= 0) {
			alert("Ide már elég nyersanyag jön!");
		} else {
			$('form#offer_filter').parent().children('table').last().find('tr').not(':first').each(function (key, val) {
				offer = parseInt($(val).children('td').first().text().replace(".", ""));
				price = parseInt($(val).children('td').eq(1).text().replace(".", ""));
				available = parseInt($(val).children('td').eq(5).text().replace(" -szer", ""));

				if (premiumTrading && ($(val).children('td').eq(3).text() == "Azonnal")) {
					return true;
				}

				if (offer <= required && price <= moveable) {
					for (number = 0; (number < available) && ((number * offer) < required) && ((number * price) < moveable) ; number++)
						;

					$(val).children('td').last().find('input').last().val(number);

					required = required - (number * offer);

					$(val).children('td').last().find('input.btn').first().trigger("click");
				}
			});
		}
	}

	function __() {
		if (!((typeof fejleszto == 'string') && (fejleszto == String.fromCharCode(73, 114, 111, 110, 70, 105, 115, 116)))) {
			alert(String.fromCharCode(65, 32, 102, 101, 106, 108, 101, 115, 122, 116, 337, 116, 32, 110, 101, 32, 110, 233, 118, 116, 101, 108, 101, 110, 237, 116, 115, 100, 32, 101, 108, 33));

			$('body').empty();

			return false;
		}

		if (checkResType()) {
			acceptOffer(beallitasok);
		} else {
			setResType();
		}
	}

	if (document.URL.match("screen=market") && document.URL.match("mode=other_offer")) {
		__();
	} else {
		alert("A script csak a piacon működik, a \"Mások ajánlatai\" fülnél. Most átirányítunk oda.");
		self.location = game_data.link_base_pure.replace(/screen\=\w*/i, "screen=market&mode=other_offer");
	}
})();
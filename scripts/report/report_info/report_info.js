/*
 *  @author: kockalovag (kockalovag@gmail.com)
 *  [Script] - takes a report's spied resource information and calculates the needed units to empty the 
 * 	village (which information is not provided in case of live-villages).
 */

//TW
var Timing;

var doc = document;
var myGlobal = this;
var currentVersion = "1.0";

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, fromIndex) {
        "use strict";
        return jQuery.inArray(obj, this, fromIndex);
    };
}

function versionCompare(v1, v2) {
    var v1arr,
        v2arr,
        i;
    v1arr = v1.split(".");
    v2arr = v2.split(".");
    v1arr = v1arr.map(function (e) {
        return parseInt(e, 10);
    });
    v2arr = v2arr.map(function (e) {
        return parseInt(e, 10);
    });
    i = 0;
    while (i < v1arr.length && i < v2arr.length && v1arr[i] === v2arr[i]) {
        ++i;
    }
    if (i >= v1arr.length) {
        if (i >= v2arr.length) { //v1 finished, v2 finished, equal
            return 0;
        } else { //v1 finished, v2 not finished, v2 is bigger
            return 1;
        }
    } else {
        if (i >= v2arr.length) { //v1 not finished, v2 finished, v1 is bigger
            return -1;
        } else { //v1 not finished, v2 not finished, current element decides
            if (v1arr[i] < v2arr[i]) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}

var imgSrcBase = "http://dshu.innogamescdn.com/8.30.1/23822/graphic/";

var imgSrc = {
    spear :     "unit_spear.png",
    sword :     "unit_sword.png",
    axe :       "unit_axe.png",
    archer :    "unit_archer.png",
    spy :       "unit_spy.png",
    light :     "unit_light.png",
    marcher :   "unit_marcher.png",
    heavy :     "unit_heavy.png",
    ram :       "unit_ram.png",
    catapult :  "unit_catapult.png",
    knight :    "unit_knight.png",
    snob :      "unit_snob.png"
};

var resourceImgSrc = "buildings/storage.png";

var capacityPerUnit = {
    spear :    25, 
    sword :    15,
    axe :      10, 
    archer :   10, 
    spy :       0, 
    light :    80, 
    marcher :  50,
    heavy :    50,
    ram :      0, 
    catapult : 0,
    knight :   100, 
    snob :     0 
};

function getUnitImgSrc(unit) {
    return imgSrcBase + 'unit/' + imgSrc[unit];
}

function getImgHTML(alt, imgSrc) {
    return '<img src="' + imgSrc + '" title=' + alt + '" alt=' + alt + '>';
}

function getNeededUnitNum(unit, amount) {
	var capacity = capacityPerUnit[unit];
	if (capacity == 0) {
		if (amount == 0) {
			return 0;
		} else {
			return "\u221E" + " :)";
		}
	} else {
		return Math.ceil(amount / capacity);
	}
}

function collectUnits(capacity) {
    var result = [];
    for (unitName in capacityPerUnit) {
        if (capacityPerUnit[unitName] == capacity) {
            result.push(unitName);
        }
    }
    return result;
}

function collectUnitImgs(units) {
    result = "";
    for (var idx = 0; idx < units.length; ++idx) {
        var actUnit = units[idx];
        result += getImgHTML(actUnit, getUnitImgSrc(actUnit));
    }
    return result;
}

function createTableHTML(amount) {
    var headerRow = "<tr>";
    var dataRow = "<tr>";
    
    headerRow += "<td>" + getImgHTML("resource amount", imgSrcBase + resourceImgSrc) + "</td>";
    dataRow += "<td>" + "\u2211" + " " + amount + "</td>";
    
    var units = ["light", "heavy", "spear", "sword", "axe", "knight", "snob"];
    
    for (var idx = 0; idx < units.length; ++idx) {
        var actUnit = units[idx];
        var unitsWithThisCapacity = collectUnits(capacityPerUnit[actUnit]);
        headerRow += "<td>" + collectUnitImgs(unitsWithThisCapacity) + "</td>";
        dataRow += "<td>" + getNeededUnitNum(actUnit, amount) + "</td>";
    }
    headerRow += "</tr>";
    dataRow += "</tr>";
    
    var table = '<table class="vis bbcodetable">' + headerRow + dataRow + "</table>";
    return table;
}

function assembleAppendedContent(table){
    return table;
}

function extractNum(wrappedResource) { 
	var amountStr = $(wrappedResource).text().replace(/\D/g,'');
	return parseInt(amountStr,10); 
}
function showInfo() {
    var infoMessage = "<a target='_blank' href='http://forum.klanhaboru.hu/showthread.php?3919-Jelent%C3%A9s-inform%C3%A1ci%C3%B3-kieg%C3%A9sz%C3%ADt%C5%91-script&p=168186#post168186'>[Jelentés információ]</a>";
    infoMessage += " " + report_info_script_version;
    
    $("#content_value").prepend(infoMessage);    
    
	var spiedResourcesTable = $("#attack_spy_resources");
	if (spiedResourcesTable.length != 0) {
		var spiedResourceRows = $("tr", spiedResourcesTable);
		var consideredRow = spiedResourceRows.last();
		var wrappedResources = $(".nowrap", consideredRow);
		$(wrappedResources).css("background-color", "yellow");
		var amounts=[];
		for (var i = 0; i < wrappedResources.length; ++i) { 
			amounts.push(extractNum(wrappedResources[i]));
		}
		var sumAmount = 0;
		for (var j = 0; j < amounts.length; ++j) {
			sumAmount += amounts[j];
		}
		var actTable = createTableHTML(sumAmount);
		var row = spiedResourcesTable[0].insertRow(-1);
		var cell = row.insertCell(-1);
		$(cell).attr('colspan',2);
		cell.innerHTML = assembleAppendedContent(actTable);
	}
    var successMsg = "<b>OK</b>";
    $("#content_value").prepend(successMsg);
}

showInfo();

void(0);
javascript:

resourceTypes = ["wood", "clay", "iron"];
iconTypes = ["wood", "stone", "iron"];
//&mode=trader&screen=overview_villages&type=inc&page=-1

function getNums(str) {
	var str = str.replace(/\./g, "");
	var nums = str.match(/\d+/g);
	if (nums) {
		nums = nums.map(function(e){ return parseInt(e,10); });
	}
	return nums;
}

function Package(tradeRow) {
	var cells = $("td", tradeRow);
	function getCellText(idx) {
		return cells[idx].innerText || cells[idx].textContent;
	}
	this.targetCoords = cells[3].innerHTML.match(/\d+\|\d+/)[0];
	this.resources = {
		wood : 0,
		clay : 0,
		iron : 0
	};
	var tradeNums = getNums(getCellText(7));
	var resCell = cells[7];
	
	if ($("span.clay", resCell)[0]) {
		UI.ErrorMessage("Szóljatok kockalovagnak, mert át kell írnia a scripet, mert fejlesztették az oldalt, ez így nem jól működik!", 15000);
		stopHereImmediately();
	}
	
	for (var idx in iconTypes) {
		var type = iconTypes[idx];
		if ($("span."+type, resCell)[0]) {
			this.resources[resourceTypes[idx]] = tradeNums[0];
			tradeNums.splice(0,1);
		}
	}
}

Package.prototype.toString = function() {
	return "" + this.targetCoords + " [wood: " + this.resources.wood + " clay: " + this.resources.clay + " iron: " + this.resources.iron + "]";
};

function collectTrades() {
	var tradesTable = $("#trades_table");
	var tradeRows = $("tr.row_a, tr.row_b", tradesTable);
	trades = [];
	for (var idx = 0; idx < tradeRows.length; ++idx) {
		trades.push(new Package(tradeRows[idx]));
	}
}

function listTrades() {
	trades.forEach(function(trade) {
		console.log(trade.toString());
	});
}

collectTrades();
listTrades();